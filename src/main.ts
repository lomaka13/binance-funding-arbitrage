import { NestExpressApplication } from '@nestjs/platform-express';

import { initApp, initBoot } from '@binance-test/app';

import { AppVars } from './app/app.config.vars';
import { AppModule } from './app/app.module';

(async (): Promise<void> => {
	const bootService = await initBoot({
		configVars: AppVars,
	});

	const app = await initApp<NestExpressApplication>(bootService, {
		appModule: AppModule.forRoot(bootService),
		validationPipe: {
			transform: true,
			transformOptions: { enableImplicitConversion: true },
		},
		swagger: {},
	});
	const server = app.getHttpServer();

	const logger = bootService.getLoggerService().getLogger('bootstrap');
	server.on('listening', () => {
		const { port, address } = server.address();
		logger.info({
			type: 'bootstrap',
			message: `Service is up and running [${address}:${port}]`,
		});
	});
})();
