/* eslint-disable no-console */
import { Injectable, OnModuleInit } from '@nestjs/common';
import {
	binance as Binance,
	bybit as Bybit,
	ascendex as Ascendex,
	bitget as BitGet,
	bitmex as Bitmex,
	coinex as Coinex,
	delta as Delta,
	deribit as Deribit,
	huobi as Huobi,
	gateio as Gateio,
	whitebit as Whitebit,
	woo as Woo,
	bingx as Bingx,
	pro,
} from 'ccxt';
import { maxBy, minBy } from 'lodash';

@Injectable()
export class ExchangesService implements OnModuleInit {
	private readonly binance = new Binance();

	private readonly bybit = new Bybit();

	private readonly huobi = new Huobi();

	private readonly gateio = new Gateio();

	private readonly whitebit = new Whitebit();

	private readonly ascendex = new Ascendex();

	private readonly bitmex = new Bitmex();

	private readonly coinex = new Coinex();

	private readonly delta = new Delta();

	private readonly deribit = new Deribit();

	private readonly bitget = new BitGet();

	private readonly woo = new Woo();

	private readonly bingx = new Bingx();

	async onModuleInit(): Promise<any> {
		// while (true) {
		// 	try {
		// 		const trades = await this.proB.watchOrderBook('OMNI/USDT');
		// 		if (trades.timestamp) {
		// 			console.log(new Date(trades.timestamp).toISOString());
		// 		} else {
		// 			console.log(new Date().toISOString());
		// 		}
		// 	} catch (e) {
		// 		console.log(e);
		// 	}
		// }
		// const this.proB.order
		const t = await this.fetchFundingRates();
		const norm = this.normalizeKeys(t);
		const mapped = await this.filterAndMapFundings(norm);
		console.log(t);
	}

	private normalizeKeys(obj: any): Map<string, { [key: string]: unknown }> {
		const map = new Map();
		for (const [exchange, tickers] of Object.entries(obj)) {
			for (const [key, value] of Object.entries(tickers)) {
				if (!key.endsWith('USDT')) {
					continue;
				}
				const replaced = key.replace(/10{1,}/, '');
				const val = map.get(replaced) ?? {};
				val[exchange] = value;
				map.set(replaced, val);
			}
		}
		return map;
	}

	private async filterAndMapFundings(mapIn: Map<string, { [key: string]: any }>): Promise<Map<string, { [key: string]: any }>> {
		const map = new Map<string, any>();
		for (const [ticker, valueIn] of mapIn) {
			map.set(ticker, valueIn);
			const value: any = map.get(ticker);
			const entries: [string, any][] = Object.entries(value);
			if (entries.length < 3) {
				map.delete(ticker);
				continue;
			}
			await this.fetchFundingAtomary(entries, ticker, 'deribit');
			await this.fetchFundingAtomary(entries, ticker, 'mexc');
			await this.fetchFundingAtomary(entries, ticker, 'lbank');
			await this.fetchFundingAtomary(entries, ticker, 'bitget');
			await this.fetchFundingAtomary(entries, ticker, 'bingx');
			for (const [exchange, data] of entries) {
				value[exchange] = {
					info: data.info,
					fundingRate: (data.nextFundingRate || data.fundingRate) * 100,
					nowFundingRate: data.fundingRate * 100,
					fundingDatetime: data.fundingDatetime,
					nextFundingRate: data.nextFundingRate ? data.nextFundingRate * 100 : undefined,
					previousFundingRate: data.previousFundingRate ? data.previousFundingRate * 100 : undefined,
					exchange,
				};
			}
			value.counters = {
				max: maxBy(Object.values(value), 'fundingRate'),
				min: minBy(Object.values(value), 'fundingRate'),
			};
			value.counters.spread = this.calculateSpread(value.counters.max.fundingRate, value.counters.min.fundingRate);
			if (value.counters.spread !== Infinity && Math.abs(value.counters.spread) > 9) {
				map.set(ticker, value);
			} else {
				map.delete(ticker);
			}
		}
		return map;
	}

	private calculateSpread(max: number, min: number): number {
		return (max - min) / max;
	}

	private async fetchFundingAtomary(entries: any[], ticker: string, exchangeKey: string): Promise<void> {
		try {
			const data = await this[exchangeKey].fetchFundingRate(ticker);
			entries.push([exchangeKey, data]);
		} catch (e) {
			/* empty */
		}
	}

	async fetchFundingRates(): Promise<{ [key: string]: unknown }> {
		return {
			binance: (await this.binance.fetchFundingRates().catch(console.error)) || {},
			bybit: (await this.bybit.fetchFundingRates().catch(console.error)) || {},
			huobi: (await this.huobi.fetchFundingRates().catch(console.error)) || {},
			gateio: (await this.gateio.fetchFundingRates().catch(console.error)) || {},
			whitebit: (await this.whitebit.fetchFundingRates().catch(console.error)) || {},
			// ascendex: (await this.ascendex.fetchFundingRates().catch(console.error)) || {},
			// bitmex: (await this.bitmex.fetchFundingRates().catch(console.error)) || {},
			// coinex: (await this.coinex.fetchFundingRates().catch(console.error)) || {},
			// delta: (await this.delta.fetchFundingRates().catch(console.error)) || {},
			woo: (await this.woo.fetchFundingRates().catch(console.error)) || {},
		};
	}
}
