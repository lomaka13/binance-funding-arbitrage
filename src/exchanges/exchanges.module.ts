import { Module, Global } from '@nestjs/common';

import { ExchangesService } from './exchanges.service';

@Global()
@Module({
	providers: [ExchangesService],
	exports: [ExchangesService],
})
export class ExchangesModule {}
