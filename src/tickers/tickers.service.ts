import { Injectable, Inject } from '@nestjs/common';

import { InjectLogger, Logger } from '@binance-test/logger';
import { RedisService } from '@binance-test/redis';
import { InjectRepository, Repository, Between } from '@binance-test/typeorm';

import { TickersEntity, SymbolsEntity } from '../entities';

@Injectable()
export class TickersService {
	constructor(
		@InjectLogger('tickers-service') private readonly logger: Logger,
		@InjectRepository(SymbolsEntity) private readonly symbolsRepository: Repository<SymbolsEntity>,
		@InjectRepository(TickersEntity) private readonly tickersRepository: Repository<TickersEntity>,
		@Inject(RedisService) private readonly redis: RedisService,
	) {}

	async list(): Promise<any[]> {
		const redisClient = this.redis.getClient();
		const keys = await redisClient.keys('prices:*');
		const raws = await redisClient.mget(keys);
		return raws.map((price: string, index: number) => ({
			symbol: keys[index].replace('prices:', ''),
			price,
		}));
	}

	async oneBySymbol(symbol: string): Promise<any> {
		const redisClient = this.redis.getClient();
		const price = await redisClient.get(`prices:${symbol}`);
		return { symbol, price };
	}

	async listHistorical(from: Date, to: Date): Promise<SymbolsEntity[]> {
		const symbols = await this.symbolsRepository.find({
			select: { symbol: true, id: true },
		});
		for (const symbol of symbols) {
			symbol.prices = await this.tickersRepository.find({
				where: { symbol, timestamp: Between(from, to) },
			});
		}
		return symbols;
	}

	async oneHistoricalBySymbol(symbol: string, from: Date, to: Date): Promise<SymbolsEntity> {
		const symbolEntity = await this.symbolsRepository.findOneOrFail({
			select: { id: true },
			where: { symbol },
		});
		symbolEntity.prices = await this.tickersRepository.find({
			where: { symbol: symbolEntity, timestamp: Between(from, to) },
		});
		return symbolEntity;
	}

	async addPrice(symbol: string, price: string, timestamp: Date): Promise<TickersEntity> {
		symbol = symbol.toLowerCase();
		try {
			return this.tickersRepository.save(
				this.tickersRepository.create({
					symbol: await this.symbolsRepository.findOneBy({ symbol }),
					price,
					timestamp,
				}),
			);
		} catch (error) {
			this.logger.error({ type: 'ERROR', error, message: 'Cannot save price' });
		}
	}
}
