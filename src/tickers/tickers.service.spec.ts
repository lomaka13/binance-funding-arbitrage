import { Test, TestingModule } from '@nestjs/testing';

import { LoggerGlobalModuleMock } from '@binance-test/mocks/logger';
import { TypeOrmMock, getRepositoryToken } from '@binance-test/mocks/typeorm';
import { RedisService } from '@binance-test/redis';

import { SymbolsEntity, TickersEntity } from '../entities';

import { TickersService } from './tickers.service';

describe('TickersService', () => {
	let service: TickersService;
	const symbolsRepository = new TypeOrmMock(SymbolsEntity).factory();
	const tickersRepository = new TypeOrmMock(TickersEntity).factory();

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			imports: [LoggerGlobalModuleMock.register()],
			providers: [
				TickersService,
				{ provide: getRepositoryToken(SymbolsEntity), useValue: symbolsRepository },
				{ provide: getRepositoryToken(TickersEntity), useValue: tickersRepository },
				{ provide: RedisService, useValue: jest.fn() },
			],
		}).compile();

		service = module.get<TickersService>(TickersService);
	});

	it('should be defined', () => {
		expect(service).toBeDefined();
	});
});
