import { Get, Param } from '@nestjs/common';

import { ApiController, DatesDto } from '@binance-test/api';
import { ReturnTypeOfClassMethod } from '@binance-test/fixtures';

import { TickersService } from './tickers.service';

@ApiController('tickers')
export class TickersController {
	constructor(private readonly service: TickersService) {}

	@Get()
	async list(): ReturnTypeOfClassMethod<TickersService, 'listHistorical'> {
		return this.service.list();
	}

	@Get('/:symbol')
	async oneBySymbol(@Param('symbol') symbol: string): ReturnTypeOfClassMethod<TickersService, 'oneBySymbol'> {
		return this.service.oneBySymbol(symbol);
	}

	@Get('/historical/:fromDate/:tillDate')
	async listHistorical(@Param() { fromDate, tillDate }: DatesDto): ReturnTypeOfClassMethod<TickersService, 'listHistorical'> {
		return this.service.listHistorical(new Date(fromDate), new Date(tillDate));
	}

	@Get('/historical/:symbol/:fromDate/:tillDate')
	async oneHistoricalBySymbol(
		@Param('symbol') symbol: string,
		@Param() { fromDate, tillDate }: DatesDto,
	): ReturnTypeOfClassMethod<TickersService, 'oneHistoricalBySymbol'> {
		return this.service.oneHistoricalBySymbol(symbol, new Date(fromDate), new Date(tillDate));
	}
}
