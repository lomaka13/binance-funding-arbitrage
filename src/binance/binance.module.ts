import { Module } from '@nestjs/common';

import { SymbolsModule } from '../symbols/symbols.module';
import { TickersModule } from '../tickers/tickers.module';

import { BinanceService } from './binance.service';

@Module({
	imports: [SymbolsModule, TickersModule],
	providers: [BinanceService],
})
export class BinanceModule {}
