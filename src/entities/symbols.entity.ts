import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToMany } from 'typeorm';

import { TickersEntity } from './tickers.entity';

@Entity('symbols')
export class SymbolsEntity {
	@PrimaryGeneratedColumn('uuid')
	id: string;

	@Column('varchar', { unique: true })
	symbol: string;

	@OneToMany(() => TickersEntity, ({ price }: TickersEntity) => price)
	prices: TickersEntity[];

	@CreateDateColumn()
	createdAt: Date;

	@UpdateDateColumn()
	updatedAt: Date;
}
