import * as path from 'path';

import { Global, Module } from '@nestjs/common';

import { TypeOrmGlobalModule } from '@binance-test/typeorm';

import { SymbolsEntity } from './symbols.entity';
import { TickersEntity } from './tickers.entity';

@Global()
@Module({
	imports: [
		TypeOrmGlobalModule.registerGlobalEntities({
			migrationsDir: path.resolve(__dirname, '..', 'migrations'),
			entities: [SymbolsEntity, TickersEntity],
		}),
	],
})
export class EntitiesModule {}
