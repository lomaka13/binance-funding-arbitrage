import { Inject, Injectable } from '@nestjs/common';

import { RedisService } from '@binance-test/redis';
import { InjectRepository, Repository, DeleteResult } from '@binance-test/typeorm';

import { SYMBOLS_CHANNEL } from '../app/app.constants';
import { SymbolsEntity } from '../entities';

@Injectable()
export class SymbolsService {
	constructor(
		@InjectRepository(SymbolsEntity) private readonly repository: Repository<SymbolsEntity>,
		@Inject(RedisService) private readonly redis: RedisService,
	) {}

	async list(): Promise<SymbolsEntity[]> {
		return this.repository.find();
	}

	async addSymbol(symbol: string): Promise<SymbolsEntity> {
		symbol = symbol.toLowerCase();
		const created = await this.repository.save(this.repository.create({ symbol }));
		this.updateInRedis(created.symbol, 'created');
		return created;
	}

	async removeSymbol(symbol: string): Promise<DeleteResult> {
		symbol = symbol.toLowerCase();
		const removed = await this.repository.delete({ symbol });
		if (removed.affected) {
			this.updateInRedis(symbol, 'removed');
		}
		return removed;
	}

	updateInRedis(symbol: string, action: 'created' | 'removed'): void {
		const redisClient = this.redis.getClient('publisher');
		redisClient.publish(SYMBOLS_CHANNEL, `${symbol}:${action}`);
	}
}
