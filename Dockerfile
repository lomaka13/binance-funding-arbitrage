ARG BASE_IMAGE
# Start build
FROM ${BASE_IMAGE:-node:18} as build

WORKDIR /build

COPY . ./

RUN npm i && npm run build
# End build

# Start install dependencies
FROM ${BASE_IMAGE:-node:18} as modules

WORKDIR /modules

COPY .npmrc ./
COPY tsconfig.json ./
COPY package.json ./
COPY package-lock.json ./

RUN npm i --production
# End install dependencies

# Start collect full image
FROM ${BASE_IMAGE:-node:18}
ARG APP_VERSION
ENV APP_PORT="3000"
ENV APP_HOST="0.0.0.0"

COPY --from=modules /modules/node_modules ./node_modules
COPY --from=build /build/dist ./dist

EXPOSE $APP_PORT
USER node
CMD node dist/main
# End collect full image
