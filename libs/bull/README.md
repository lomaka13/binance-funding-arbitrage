# Bull-based queue

WARNING! Do not move "bull" from service package.json to common, in this case service will fail to start. See: https://github.com/OptimalBits/bull/issues/920

### Usage example

Add queue flow to module

```ts
export class QueueService extends BullQueueService {
	constructor(
		@InjectQueue('test') readonly queue: Queue,
	) {
		super();
	}
}

@Processor('test')
export class QueueProcessorService extends BullLoggerService {
	constructor(
		@InjectLogger('test-queue') readonly logger: Logger,
	) {
		super();
	}

	@Process()
	async runJob(job: Job<any>): Promise<void> {
		// process job
	}
}

@Module({
	module: AppModule,
	providers: [
		QueueService,
		QueueProcessorService,
	],
	imports: [
		BullModule.registerQueue({
			name: 'test',
		}),
	],
})
export class AppModule {}
```

