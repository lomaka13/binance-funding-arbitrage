import { OnQueueFailed, OnQueueError, OnQueueStalled, OnQueueCompleted } from '@nestjs/bull';
import { Job } from 'bull';

import { Logger } from '@binance-test/logger';

export const logTypes = {
	NOTIFY: 'notify_event',
	ERROR: 'error_event',
	FAIL: 'fail_event',
	STALE: 'stale_event',
};

export abstract class BullLoggerService {
	protected abstract readonly logger: Logger;

	abstract runJob(job: any): Promise<any>;

	@OnQueueFailed()
	queueFailed(job: Job, error: Error): void {
		const jobId = String(job.id);
		this.logger.error({
			type: logTypes.FAIL,
			message: `Job ${jobId} queue failed: ${error.message}`,
			jobId,
			error,
		});
	}

	@OnQueueError()
	queueError(error: Error): void {
		this.logger.error({
			type: logTypes.ERROR,
			message: `Queue error: ${error.message}`,
			error,
		});
	}

	@OnQueueStalled()
	queueStalled(job: Job): void {
		const jobId = String(job.id);
		this.logger.warn({
			type: logTypes.STALE,
			message: `Job ${jobId} stalled`,
			jobId,
		});
	}

	@OnQueueCompleted()
	queueCompleted(job: Job, result: any): void {
		const jobId = String(job.id);
		this.logger.info({
			type: logTypes.NOTIFY,
			message: `Job ${jobId} completed`,
			jobId,
			result,
		});
	}
}
