import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';

import { initBoot } from '@binance-test/app';

import { AppModule } from './bull-test-many.module';
import { BullManagementService } from '../src';

describe.skip('Libs > Bull (many) (e2e)', () => {
    let app: INestApplication;
    let mgmtService: BullManagementService;

	beforeAll(async () => {
		const bootService = await initBoot({
			configVars: {},
		});
		const moduleFixture: TestingModule = await Test.createTestingModule({
			imports: [
				AppModule.forRoot(bootService),
			],
		})
		.compile();

		app = moduleFixture.createNestApplication();
        await app.init();

        mgmtService = app.get<BullManagementService>(BullManagementService);
	});

	beforeEach(async () => {
		jest.clearAllTimers();
		jest.clearAllMocks();
	});

	afterAll(async () => {
		await app.close();
	});

	it('check queues list', async () => {
        const queueMap = mgmtService.list();
        expect(Array.from(queueMap.keys())).toEqual(['test', 'test2']);
    });

    it('check one by one', async () => {
        const queue = mgmtService.getOne('test');
        expect(queue).toBeTruthy();
        expect(queue?.name).toBe('test');

        const queue2 = mgmtService.getOne('test2');
        expect(queue2).toBeTruthy();
        expect(queue2?.name).toBe('test2');
    });

});
