import { DynamicModule, Module } from '@nestjs/common';

import { BootServiceInterface } from '@binance-test/boot';
import { LoggerGlobalModule } from '@binance-test/logger';
import { ConfigGlobalModule } from '@binance-test/config';

import { BullModule } from '../src';

const redis = {
	host: 'localhost',
	port: 6379,
	password: 'qwerty123',
};

@Module({})
export class AppModule {
	public static forRoot(bootService: BootServiceInterface): DynamicModule {
		return {
			module: AppModule,
			imports: [
				ConfigGlobalModule.forRoot(bootService),
				LoggerGlobalModule.forRoot(bootService),
				BullModule.registerManagementQueues([
					{
						name: 'test',
						queueOptions: { redis },
					},
					{
						name: 'test2',
						queueOptions: { redis },
					},
				]),
			],
		};
	}
}
