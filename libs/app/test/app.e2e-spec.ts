import * as request from 'supertest';
import { Server } from 'http';
import { INestApplication } from '@nestjs/common';

import { AppCommonVars } from '@binance-test/app';
import { initBoot, initApp } from '@binance-test/test-utils/bootstrap';
import '@binance-test/test-utils';

import { LoggerGlobalModuleMock } from '@binance-test/logger/mock';

import { AppModule } from './fixtures/app.module';

import './test.env';
jest.setTimeout(30000);

describe('App (e2e)', () => {

	let server: Server;
	let app: INestApplication;

	beforeAll(async () => {
		const bootService = await initBoot({
			configVars: { ...AppCommonVars },
			loggerModule: LoggerGlobalModuleMock.register(),
		});
		app = await initApp(bootService, {
			testModule: {
				imports: [
					AppModule.forRoot(bootService),
				],
			},
		});

		server = app.getHttpServer();
		await app.init();
	});

	afterAll(async () => {
		await app.close();
	});

	describe('Common app e2e', () => {

		it('should expose GET /info', async () => {
			const res = await request(server).get('/info').expect(200);
			expect(res.body).toStrictEqual({
				status: 200,
				data: {
					name: 'app.e2e',
					version: 'local',
					env: 'test',
					memory: expect.anything(),
				},
			});
		});
	});
});
