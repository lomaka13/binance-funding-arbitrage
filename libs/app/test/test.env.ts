process.env.NODE_ENV = 'test';
process.env.APP_NAME = 'app.e2e';
process.env.APP_VERSION = 'local';
process.env.APP_PORT = '3000';
process.env.APP_HOST = 'localhost';
process.env.APP_LOG_LEVEL = process.env.APP_LOG_LEVEL || 'fatal';
process.env.APP_LOG_LEVEL = process.env.APP_LOG_LEVEL || 'fatal';
process.env.APP_CLIENT_VERSION = '0.0.1';
