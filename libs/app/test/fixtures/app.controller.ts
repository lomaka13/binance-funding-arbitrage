import { Controller } from '@nestjs/common';

import { AppController as AppBaseController } from '@binance-test/app';

import { AppService } from './app.service';

@Controller()
export class AppController extends AppBaseController {

	constructor(protected appService: AppService) {
		super(appService);
	}
}
