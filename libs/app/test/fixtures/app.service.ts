import { Injectable } from '@nestjs/common';

import { AppService as BaseAppService } from '@binance-test/app';
import { ConfigService } from '@binance-test/config';
import { InjectLogger, Logger } from '@binance-test/logger';

@Injectable()
export class AppService extends BaseAppService {

	constructor(
		protected configService: ConfigService,
		@InjectLogger('app-service-logger') protected logger: Logger,
	) {
		super(configService);
	}

	logString(message: string): void {
		this.logger.info({
			type: 'app_log_item',
			message,
		});
	}
}
