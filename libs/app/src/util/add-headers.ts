import { Response } from 'express';

/**
 * Adds only new response headers
 */
export const addHeaders = (response: Response, headers: any): void => {
	Object.entries(headers).forEach(([name, value]: [string, string | string[]]) => {
		if (!response.hasHeader(name)) {
			response.setHeader(name, value);
		}
	});
};
