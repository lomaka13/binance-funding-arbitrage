import { LogMessage } from '@binance-test/logger';

import { REQUEST_START_TIME, REQUEST_ID, REQUEST_PATH } from '../app.constants';
import { Request } from '../app.types';

/**
 * Get request info object
 */
export const getRequestInfo = (request: Request, includeExtraInfo: boolean): Omit<LogMessage, 'type'> => {
	const { method, url, httpVersion, user, body, query, params, headers } = request;
	const start = request[REQUEST_START_TIME];
	const rid = request[REQUEST_ID];
	const methodPath = request[REQUEST_PATH];
	return {
		...(rid && { rid }),
		method,
		url,
		httpVersion,
		...(start && {
			start,
			duration: Date.now() - start,
		}),
		...(user && { user }),
		...(includeExtraInfo && {
			params,
			query,
			headers,
			body,
		}),
		methodPath,
	};
};
