import { memoryUsage } from 'process';

import { Injectable } from '@nestjs/common';

import { ConfigService } from '@binance-test/config';

import { AppCommonVarsType } from './app.vars';

@Injectable()
export class AppService {
	constructor(protected configService: ConfigService<AppCommonVarsType>) {}

	getName(): string {
		return this.configService.get('APP_NAME');
	}

	getVersion(): string {
		return this.configService.get('APP_VERSION');
	}

	getInfo(): {
		name: string;
		version: string;
		env: string;
		memory: ReturnType<typeof memoryUsage>;
	} {
		return {
			name: this.getName(),
			version: this.getVersion(),
			env: this.configService.env,
			memory: memoryUsage(),
		};
	}
}
