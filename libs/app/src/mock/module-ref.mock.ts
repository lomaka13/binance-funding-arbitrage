import { Type } from '@nestjs/common';
import { ContextId, ModuleRef, NestContainer } from '@nestjs/core';

export class ModuleRefMock extends ModuleRef {
	// @ts-ignore
	// eslint-disable-next-line @typescript-eslint/ban-types
	get<TInput = any, TResult = TInput>(typeOrToken: string | symbol | Function | Type<TInput>, options?: { strict: boolean }): TResult {
		return this.mocksMap.get(['symbol', 'string'].includes(typeof typeOrToken) ? typeOrToken : (typeOrToken as any).name) as any;
	}

	// @ts-ignore
	resolve<TInput = any, TResult = TInput>(
		// eslint-disable-next-line @typescript-eslint/ban-types
		typeOrToken: string | symbol | Function | Type<TInput>,
		contextId?: ContextId,
		options?: { strict: boolean },
	): Promise<TResult> {
		return this.mocksMap.get(['symbol', 'string'].includes(typeof typeOrToken) ? typeOrToken : (typeOrToken as any).name) as any;
	}

	create<T = any>(type: Type<T>): Promise<T> {
		this.mocksMap.set(type.name, type as any);
		return type.prototype;
	}

	public container: NestContainer;

	private readonly mocksMap: Map<string, jest.Mock> = new Map();

	constructor(container: NestContainer = new NestContainer()) {
		super(container);
		this.container = container;
	}
}
