export const REQUEST_START_TIME = Symbol();
export const REQUEST_ID = Symbol();
export const REQUEST_LOGGER = Symbol();
export const REQUEST_PATH = Symbol();

export const REQUEST_LOGGER_NAME = 'app-request';

export const INFINITY_AMOUNT_VALUE = 'Infinity';
