import { Request as ExpressRequest } from 'express';

import { Logger } from '@binance-test/logger';

import { REQUEST_START_TIME, REQUEST_ID, REQUEST_LOGGER, REQUEST_PATH } from './app.constants';

export interface Request<T = any> extends ExpressRequest {
	user: T;
	[REQUEST_START_TIME]: number;
	[REQUEST_ID]: string;
	[REQUEST_LOGGER]: Logger;
	[REQUEST_PATH]: string;
}

export { Response } from 'express';
