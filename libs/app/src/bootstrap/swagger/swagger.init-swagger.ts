import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from '@nestjs/swagger';
import { SecuritySchemeObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { Request, Response } from 'express';
import { cloneDeep, flatten, has, map, pick, omitBy, get } from 'lodash';

import { API_VERSION } from '@binance-test/api';
import { ConfigService } from '@binance-test/config';

import { AppCommonVarsType } from '../../app.vars';

interface SwaggerAuthMethods {
	method: keyof Pick<DocumentBuilder, 'addBearerAuth' | 'addOAuth2' | 'addApiKey' | 'addBasicAuth'>;

	name?: string;

	scheme?: SecuritySchemeObject;
}

export interface SwaggerOptions {
	title?: string;

	description?: string;

	version?: string;

	authorization?: SwaggerAuthMethods[];
}

interface SchemaPath {
	value: string;

	path: string;
}

function findSchemaUsage(obj: any, key: string, value: string, path: string[] = []): SchemaPath[] {
	if (has(obj, key) && obj[key].startsWith(value)) {
		return [{ value: obj[key], path: path.join('.') }];
	}

	return flatten(
		map(obj, (v: any, k: string) => {
			return typeof v == 'object' ? findSchemaUsage(v, key, value, path.concat([k])) : [];
		}),
	);
}

export const filterByVersion = (doc: OpenAPIObject, version: API_VERSION): OpenAPIObject => {
	const document = cloneDeep(doc);
	document.paths = omitBy(document.paths, (value: any, key: string) => !key.startsWith(`/${version}`));
	const needSaveSchema: Set<string> = new Set<string>();
	const recursiveFindSchemaForSchema = (schemaNamePath: string): void => {
		const schema = get(document, schemaNamePath);
		const inCurrentSchema = findSchemaUsage(schema, '$ref', '#/components/schemas');
		for (const { value } of inCurrentSchema) {
			const schemaNamePath = value.replace('#/', '').replace(/\//g, '.');
			needSaveSchema.add(schemaNamePath);
			recursiveFindSchemaForSchema(schemaNamePath);
		}
	};
	for (const path of Object.keys(document.paths)) {
		const currentPath: object = (
			document.paths as {
				[index: string]: object;
			}
		)[path];
		const pathSchemas = findSchemaUsage(currentPath, '$ref', '#/components/schemas');
		for (const { value } of pathSchemas) {
			const schemaNamePath = value.replace('#/', '').replace(/\//g, '.');
			needSaveSchema.add(schemaNamePath);
			recursiveFindSchemaForSchema(schemaNamePath);
		}
	}

	document.components.schemas = pick(document, [...needSaveSchema]).components?.schemas;
	return document;
};

/**
 * Init swagger
 */
export const initSwagger = (
	app: INestApplication,
	configService: ConfigService<AppCommonVarsType>,
	options: SwaggerOptions,
	metricPath: string,
): OpenAPIObject => {
	const { title, description, version, authorization } = options;
	const path = '/swagger';
	const appName = configService.get('APP_NAME');
	const swaggerOptions = new DocumentBuilder()
		.setTitle(title || `${appName} service API`)
		.setDescription(description || `${appName} service API swagger document`)
		.setVersion(version || configService.get('APP_VERSION'));

	if (authorization) {
		authorization.forEach((auth: SwaggerAuthMethods) => {
			swaggerOptions[auth.method](auth.scheme, auth.name);
		});
	}

	const document = SwaggerModule.createDocument(app, swaggerOptions.build());
	// add default headers to all routes
	delete document.paths[metricPath];
	const clientApiUrl = `${path}-json/api`;
	const clientDocument = filterByVersion(document, API_VERSION.API);
	const devOnlyApiUrl = `${path}-json/dev`;
	const devOnlyDocument = filterByVersion(document, API_VERSION.DEV);
	const urls = [{ name: 'Client API', url: clientApiUrl }];
	if (configService.isDev) {
		urls.push({ name: 'Dev-only API', url: devOnlyApiUrl });
	}

	SwaggerModule.setup(path, app, {} as any, {
		customCss: `.swagger-ui .topbar .download-url-wrapper { display: inherit !important }`,
		swaggerOptions: { url: '/swagger-json/api', urls },
	});
	app.use(clientApiUrl, (req: Request, res: Response) => res.send(clientDocument));
	if (configService.isDev) {
		app.use(devOnlyApiUrl, (req: Request, res: Response) => res.send(devOnlyDocument));
	}

	return document;
};
