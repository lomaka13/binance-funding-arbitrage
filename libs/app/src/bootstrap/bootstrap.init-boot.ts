import { DynamicModule } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import { BootModule, BootServiceInterface, BootService } from '@binance-test/boot';
import { ConfigModule, ConfigService } from '@binance-test/config';
import { LoggerModule, createLogger, LogMessageSafeField } from '@binance-test/logger';

import { AppCommonVarsType } from '../app.vars';

export interface BootOptions {
	configVars: object; // to validate presence
	configModule?: DynamicModule; // to override config module for testing
	loggerModule?: DynamicModule; // to override logger module for testing
}

/**
 * Init boot module to start bootstrapping
 */
export const initBoot = async (options: BootOptions): Promise<BootServiceInterface> => {
	const { configVars, configModule, loggerModule } = options;
	const nonSafeDataFields: LogMessageSafeField[] = ['data', 'result', 'error', 'params', 'body', 'query'];
	const config = configModule || ConfigModule.register({});
	const logger =
		loggerModule ||
		LoggerModule.registerAsync({
			imports: [config],
			inject: [ConfigService],
			useFactory: createLogger,
			loggerConfig: {
				inspectDepth: 7,
				nonSafeDataFields,
				maxStringLength: 10_000,
			},
		});
	const bootModule = BootModule.register({
		imports: [config, logger],
	});
	const boot = await NestFactory.createApplicationContext(bootModule, {
		logger: false,
	});
	const bootService = boot.get(BootService);
	const configService = bootService.getConfigService<AppCommonVarsType>();

	bootService.enableProcessExceptionsHooks();
	configService.ensureConfig(configVars);

	return bootService;
};
