import * as path from 'path';

import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as uuid from 'uuid';

import { RequestHeader, ResponseHeader } from '@binance-test/api';
import { LoggerService, LoggerOptions, LogMessage, LogType } from '@binance-test/logger';

import { REQUEST_START_TIME, REQUEST_ID, REQUEST_LOGGER, REQUEST_PATH } from './app.constants';
import { Request, Response } from './app.types';
import { addHeaders, getRequestInfo } from './util';

export interface AppInterceptorOptions {
	metricPath: string;
}

/**
 * Interceptor for common app headers processing and response formatting
 */
@Injectable()
export class AppInterceptor implements NestInterceptor {
	private reflector: Reflector = new Reflector();

	constructor(private loggerService: LoggerService, private loggerOptions: LoggerOptions, private interceptorOptions: AppInterceptorOptions) {}

	public intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
		const request: Request = context.switchToHttp().getRequest();
		const response: Response = context.switchToHttp().getResponse();

		request[REQUEST_ID] = request.get(RequestHeader.REQUEST_ID) || uuid.v4();
		request[REQUEST_START_TIME] = Date.now();
		request[REQUEST_LOGGER] = this.loggerService.wrapRequest(request[REQUEST_ID], this.loggerOptions);
		request[REQUEST_PATH] = path.join(
			...this.reflector.getAllAndMerge('path', [context.getClass(), context.getHandler()]).map((p: string) => `/${p}`),
		);
		const metricPath = this.interceptorOptions.metricPath.replace(/[/\\]/g, '');
		const url = request.url.replace(/[/\\]/g, '');
		if (![metricPath, 'info', 'version', 'health-check'].includes(url)) {
			request[REQUEST_LOGGER].info({
				...getRequestInfo(request, true),
				type: LogType.LOG_MESSAGE,
				message: 'HTTP request',
			});
		}

		request.headers[RequestHeader.REQUEST_ID] = request[REQUEST_ID];
		response.set(ResponseHeader.REQUEST_ID, request[REQUEST_ID]);

		return next.handle().pipe(
			map((result: any) => {
				const metricPath = this.interceptorOptions.metricPath.replace(/[/\\]/g, '');
				const url = request.url.replace(/[/\\]/g, '');
				if (metricPath) {
					return result;
				}
				const transformedResult = this.response(response, result, context.getHandler());
				const responseDataField = request.headers[RequestHeader.RESPONSE_DATA_LOG];

				if (!['info', 'version', 'health-check'].includes(url)) {
					request[REQUEST_LOGGER].info(
						AppInterceptor.getResponseInfo(request, response, transformedResult, false, responseDataField ? String(responseDataField) : ''),
					);
					request[REQUEST_LOGGER].trace(AppInterceptor.getResponseInfo(request, response, transformedResult, true));
				}
				return transformedResult;
			}),
		);
	}

	// eslint-disable-next-line @typescript-eslint/ban-types
	private response(response: Response, result: unknown, handler: Function): { status: number; data: any } {
		const status: number = response.statusCode;
		let headers: any | undefined;
		let data: any;

		if (!result) {
			data = {};
		} else if (typeof result === 'string') {
			data = { message: result };
		} else {
			data = result || {};
		}

		response.status(status);
		if (headers) {
			addHeaders(response, headers);
		}

		return {
			status,
			data,
		};
	}

	/**
	 * Construct and return request-response event info object
	 */
	private static getResponseInfo(
		request: Request,
		response: Response,
		result: { status: number; data: any },
		includeExtraInfo: boolean,
		responseDataField?: string,
	): LogMessage {
		const requestInfo = getRequestInfo(request, includeExtraInfo);
		const { statusCode: httpStatus } = response;

		return {
			type: includeExtraInfo ? LogType.HTTP_OK_EXT : LogType.HTTP_OK,
			message: `HTTP ${httpStatus} response`,
			...requestInfo,
			httpStatus,
			...(includeExtraInfo && { result }),
			...(responseDataField && { [responseDataField]: result.data }),
		};
	}
}
