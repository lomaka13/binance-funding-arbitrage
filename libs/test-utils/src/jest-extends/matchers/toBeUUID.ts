import { matcherHint, printReceived } from 'jest-matcher-utils';

const passMessage =
	(received: any): (() => string) =>
	(): string =>
		matcherHint('.not.toBeUUID', 'received', '') + '\n\n' + 'Expected value to not be UUID, received:\n' + `  ${printReceived(received)}`;

const failMessage =
	(received: any): (() => string) =>
	(): string =>
		matcherHint('.toBeUUID', 'received', '') + '\n\n' + 'Expected value to be UUID, received:\n' + `  ${printReceived(received)}`;

export const toBeUUID = (received: any): jest.CustomMatcherResult => {
	const re = /^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}$/;
	const pass = typeof received === 'string' && re.test(received);

	if (pass) {
		return { pass: true, message: passMessage(received) };
	}

	return { pass: false, message: failMessage(received) };
};
