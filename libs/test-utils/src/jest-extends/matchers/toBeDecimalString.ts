import { matcherHint, printReceived } from 'jest-matcher-utils';

import { isNumeric } from './utils';

const passMessage =
	(received: any): (() => string) =>
	(): string =>
		matcherHint('.not.toBeNumericString', 'received', '') +
		'\n\n' +
		'Expected value to not be numeric string, received:\n' +
		`  ${printReceived(received)}`;

const failMessage =
	(received: any): (() => string) =>
	(): string =>
		matcherHint('.toBeNumericString', 'received', '') + '\n\n' + 'Expected value to be numeric string, received:\n' + `  ${printReceived(received)}`;

export const toBeDecimalString = (received: any): jest.CustomMatcherResult => {
	const pass = isNumeric(received);
	if (pass) {
		return { pass: true, message: passMessage(received) };
	}

	return { pass: false, message: failMessage(received) };
};
