import { matcherHint, printReceived } from 'jest-matcher-utils';

import { isInt } from './utils';

const passMessage =
	(received: any): (() => string) =>
	(): string =>
		matcherHint('.not.toBePositiveIntegerString', 'received', '') +
		'\n\n' +
		'Expected value to not be positive integer string, received:\n' +
		`  ${printReceived(received)}`;

const failMessage =
	(received: any): (() => string) =>
	(): string =>
		matcherHint('.toBePositiveIntegerString', 'received', '') +
		'\n\n' +
		'Expected value to be positive integer string, received:\n' +
		`  ${printReceived(received)}`;

export const toBePositiveIntegerString = (received: any): jest.CustomMatcherResult => {
	const pass = isInt(received) && parseInt(received, 10) >= 0;
	if (pass) {
		return { pass: true, message: passMessage(received) };
	}

	return { pass: false, message: failMessage(received) };
};
