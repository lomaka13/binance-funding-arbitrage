import { matcherHint, printReceived } from 'jest-matcher-utils';

import { isInt } from './utils';

const passMessage =
	(received: any): (() => string) =>
	(): string =>
		matcherHint('.not.toBeIntegerString', 'received', '') +
		'\n\n' +
		'Expected value to not be integer string, received:\n' +
		`  ${printReceived(received)}`;

const failMessage =
	(received: any): (() => string) =>
	(): string =>
		matcherHint('.toBeIntegerString', 'received', '') + '\n\n' + 'Expected value to be integer string, received:\n' + `  ${printReceived(received)}`;

export const toBeIntegerString = (received: any): jest.CustomMatcherResult => {
	const pass = isInt(received);
	if (pass) {
		return { pass: true, message: passMessage(received) };
	}

	return { pass: false, message: failMessage(received) };
};
