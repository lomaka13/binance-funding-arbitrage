import { matcherHint, printReceived } from 'jest-matcher-utils';

import { isNumeric } from './utils';

const passMessage =
	(received: any): (() => string) =>
	(): string =>
		matcherHint('.not.toBePositiveNumericString', 'received', '') +
		'\n\n' +
		'Expected value to not be positive numeric string, received:\n' +
		`  ${printReceived(received)}`;

const failMessage =
	(received: any): (() => string) =>
	(): string =>
		matcherHint('.toBePositiveNumericString', 'received', '') +
		'\n\n' +
		'Expected value to be positive numeric string, received:\n' +
		`  ${printReceived(received)}`;

export const toBePositiveDecimalString = (received: any): jest.CustomMatcherResult => {
	const pass = isNumeric(received) && parseFloat(received) >= 0;
	if (pass) {
		return { pass: true, message: passMessage(received) };
	}

	return { pass: false, message: failMessage(received) };
};
