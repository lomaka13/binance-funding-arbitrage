import * as http from 'http';

import { delay } from 'bluebird';

const DEFAULT_PORT = 56789;

const mockDefaults = {
	status: 200,
	headers: {
		// eslint-disable-next-line @typescript-eslint/naming-convention
		'content-type': 'application/json',
	},
	text: '{}',
};

const factory = async (
	port: number,
	{
		responseTimeout,
		responseStatus,
		responseHeaders,
		responseText,
	}: {
		[name: string]: jest.Mock;
	},
): Promise<http.Server> => {
	const server = http.createServer(async (req: any, res: http.ServerResponse) => {
		await delay(responseTimeout());
		res.writeHead(responseStatus(), responseHeaders());
		res.write(responseText());
		res.end();
	});

	return new Promise((resolve: any): typeof server => server.listen(port, '127.0.0.1', () => resolve(server)));
};

export interface HttpServerMockApi {
	startServer: () => Promise<http.Server>;
	closeServer: () => Promise<any>;
	mocks: {
		responseTimeout: jest.Mock;
		responseStatus: jest.Mock;
		responseHeaders: jest.Mock;
		responseText: jest.Mock;
	};
	resetMocks: () => void;
}

export const getServerMock = ({
	serverPort = DEFAULT_PORT,
	defaultStatus,
	defaultHeaders,
	defaultText,
}: {
	serverPort?: number;
	defaultStatus?: number;
	defaultHeaders?: {
		[headerName: string]: string | string[];
	};
	defaultText?: string;
} = {}): HttpServerMockApi => {
	const options = Object.assign({}, mockDefaults, {
		defaultStatus,
		defaultHeaders,
		defaultText,
	});
	const mocks = {
		responseTimeout: jest.fn(),
		responseStatus: jest.fn(),
		responseHeaders: jest.fn(),
		responseText: jest.fn(),
	};
	let server: http.Server;

	const startServer = async (): Promise<typeof server> => {
		server = await factory(serverPort, mocks);
		return server;
	};

	const closeServer = async (): Promise<void> => new Promise((resolve: any): typeof server => server.close(resolve));

	const resetMocks = (): void => {
		mocks.responseTimeout.mockReturnValue(0);
		mocks.responseStatus.mockReturnValue(options.status);
		mocks.responseHeaders.mockReturnValue(options.headers);
		mocks.responseText.mockReturnValue(options.text);
	};

	resetMocks();

	return {
		startServer,
		closeServer,
		mocks,
		resetMocks,
	};
};
