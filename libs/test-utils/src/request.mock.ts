import { random } from 'faker';

import { Request, REQUEST_LOGGER, REQUEST_ID, REQUEST_START_TIME, REQUEST_PATH } from '@binance-test/app';
import { ObjectLiteral } from '@binance-test/fixtures';
import { createLoggerMock, LoggerMock } from '@binance-test/mocks/logger';

export type RequestMock = {
	// eslint-disable-next-line @typescript-eslint/ban-types
	[P in keyof Request]: Request[P] extends Function ? jest.Mock : Request[P];
} & {
	testLogger: LoggerMock;
	testStartTime: number;
	testRequestId: string;
};

const defaultProps = {
	method: 'POST',
	url: '/url',
	[REQUEST_PATH]: 'url',
	httpVersion: 1.1,
	params: {},
	body: {},
	query: {},
	headers: {},
};

export const createRequestMock = <T extends ObjectLiteral = ObjectLiteral>(props?: T): RequestMock => {
	const testLogger = createLoggerMock();
	const testStartTime = Date.now();
	const testRequestId = random.uuid();
	return Object.assign(
		{
			[REQUEST_START_TIME]: testStartTime,
			[REQUEST_ID]: testRequestId,
			[REQUEST_LOGGER]: testLogger,
			get: jest.fn(),
			is: jest.fn(),
			param: jest.fn(),
			range: jest.fn(),
			testLogger,
			testStartTime,
			testRequestId,
		},
		defaultProps,
		props,
	);
};
