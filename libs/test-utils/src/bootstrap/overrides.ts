import { TestingModuleBuilder } from '@nestjs/testing';

import { TypeXOR, ClassType } from '@binance-test/fixtures';

export type Override = { provider: any } & TypeXOR<
	{
		useClass: ClassType<any>;
	},
	{
		useValue: any;
	}
>;

const getDefaultOverrides = (): Override[] => [];

export const applyOverrides = (builder: TestingModuleBuilder, overrides: Override[] = []): void => {
	for (const { provider, useClass, useValue } of [...getDefaultOverrides(), ...(overrides || [])]) {
		const by = builder.overrideProvider(provider);
		if (useClass) {
			by.useClass(useClass);
		} else if (useValue) {
			by.useValue(useValue);
		}
	}
};
