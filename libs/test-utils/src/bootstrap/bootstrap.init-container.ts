import { INestApplicationContext } from '@nestjs/common';
import { ModuleMetadata } from '@nestjs/common/interfaces';
import { Test } from '@nestjs/testing';

import { applyOverrides } from './overrides';

export const initContainer = async (testModule: ModuleMetadata = {}): Promise<INestApplicationContext> => {
	testModule.imports = testModule.imports || [];
	const builder = Test.createTestingModule(testModule);
	applyOverrides(builder);
	return builder.compile();
};
