import { ExecutionContext } from '@nestjs/common';
import { Request as ExpressRequest, Response as ExpressResponse } from 'express';

import { createRequestMock, RequestMock } from './request.mock';
import { createResponseMock, ResponseMock } from './response.mock';

interface ContextParams {
	requestMock?: ExpressRequest | RequestMock;
	responseMock?: ExpressResponse | ResponseMock;
}

const defaultImplementation = (): any => {
	throw new Error('Method not implemented!');
};

export type ExecutionContextMock = Record<keyof ExecutionContext, jest.Mock>;

export const createExecutionContext = (params: ContextParams): ExecutionContextMock => {
	const mocks: Required<ContextParams> = Object.assign(
		{
			requestMock: createRequestMock(),
			responseMock: createResponseMock(),
		},
		params,
	);

	return {
		getClass: jest.fn(defaultImplementation),
		getHandler: jest.fn(defaultImplementation),
		getArgs: jest.fn(defaultImplementation),
		getArgByIndex: jest.fn(defaultImplementation),
		switchToRpc: jest.fn(defaultImplementation),
		switchToHttp: jest.fn().mockReturnValue({
			getRequest: jest.fn().mockReturnValue(mocks.requestMock),
			getResponse: jest.fn().mockReturnValue(mocks.responseMock),
			getNext: jest.fn(),
		}),
		switchToWs: jest.fn(defaultImplementation),
		getType: jest.fn(defaultImplementation),
	};
};
