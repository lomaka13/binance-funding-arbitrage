import { Reflector } from '@nestjs/core';

export type ReflectorMock = Record<keyof Reflector, jest.Mock>;

export const createReflectorMock = (): ReflectorMock => ({
	get: jest.fn(),
	getAll: jest.fn(),
	getAllAndMerge: jest.fn(),
	getAllAndOverride: jest.fn(),
});
