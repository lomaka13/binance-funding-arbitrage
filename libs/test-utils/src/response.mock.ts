import { Response } from '@binance-test/app';
import { ObjectLiteral } from '@binance-test/fixtures';

export type ResponseMock = {
	// eslint-disable-next-line @typescript-eslint/ban-types
	[P in keyof Response]: Response[P] extends Function ? jest.Mock : Response[P];
};

export const createResponseMock = <T extends ObjectLiteral = ObjectLiteral>(props?: T): ResponseMock => {
	return Object.assign(
		{
			append: jest.fn(),
			attachment: jest.fn(),
			cookie: jest.fn(),
			clearCookie: jest.fn(),
			download: jest.fn(),
			end: jest.fn(),
			format: jest.fn(),
			get: jest.fn(),
			json: jest.fn(),
			location: jest.fn(),
			redirect: jest.fn(),
			send: jest.fn(),
			sendStatus: jest.fn(),
			set: jest.fn(),
			status: jest.fn(function (this: any, status: number): any {
				this.statusCode = status;
				return this;
			}),
			type: jest.fn(),
			vary: jest.fn(),
		},
		props || {},
	);
};
