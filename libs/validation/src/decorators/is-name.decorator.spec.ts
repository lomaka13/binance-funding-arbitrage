import { plainToClass } from 'class-transformer';
import { validate, ValidationError } from 'class-validator';

import { IsName } from './';

describe('Libs > Validation > Validation decorators -> IsName', () => {
	const validateValue = async (name: string): Promise<ValidationError[]> => {
		class DTO {
			@IsName(new RegExp(`^[a-z .\`‘’\',-]+$`, 'i'))
			name: string;
		}
		const object = plainToClass(DTO, { name });
		return validate(object);
	};

	it('should validate correctly simply name', async () => {
		const errors = await validateValue('Hans');
		expect(errors.length).toBe(0);
	});

	it('should validate correctly 2 words name by space', async () => {
		const errors = await validateValue('Hans Junior');
		expect(errors.length).toBe(0);
	});

	it('should validate correctly name with special chars', async () => {
		const errors = await validateValue(`Hans\`Junior-third'.`);
		expect(errors.length).toBe(0);
	});

	it('should return errors name with empty string', async () => {
		const errors = await validateValue('');
		expect(errors.length).toBe(1);
		expect(errors[0].constraints?.isName).toEqual(`name must be a valid name string by regular expression - /^[a-z .\`‘’\',-]+$/i`);
	});

	it('should return errors name with 4 spaces', async () => {
		const errors = await validateValue('    ');
		expect(errors.length).toBe(1);
		expect(errors[0].constraints?.isName).toEqual(`name must be a valid name string by regular expression - /^[a-z .\`‘’\',-]+$/i`);
	});
});
