import { registerDecorator } from 'class-validator';

export const IsName =
	(regex: RegExp): ((...args: any) => void) =>
	(object: object, propertyName: string): void => {
		registerDecorator({
			name: 'isName',
			target: object.constructor,
			propertyName,
			validator: {
				defaultMessage: ({ property }: any): string => `${property} must be a valid name string by regular expression - ${regex}`,
				validate(value: string): boolean {
					try {
						return regex.test(value.trim());
					} catch (e) {
						return false;
					}
				},
			},
		});
	};
