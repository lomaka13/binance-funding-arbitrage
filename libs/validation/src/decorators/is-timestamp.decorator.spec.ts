import { plainToClass } from 'class-transformer';
import { validate, ValidationError } from 'class-validator';

import { IsTimestamp } from './';

describe('Common > Validation > Validation decorators -> IsTimestamp', () => {
	const validateValue = async (timestamp: number, minusFromNow?: number, plusFromNow?: number): Promise<ValidationError[]> => {
		class DTO {
			@IsTimestamp(minusFromNow, plusFromNow)
			timestamp: number;
		}
		const object = plainToClass(DTO, { timestamp });
		return validate(object);
	};

	it('should validate correctly timestamp', async () => {
		const errors = await validateValue(new Date().valueOf());
		expect(errors.length).toBe(0);
	});

	it('should validate correctly timestamp with isAfter 60 seconds', async () => {
		const now = new Date().setSeconds(new Date().getSeconds() - 30).valueOf();
		const errors = await validateValue(now, 60);
		expect(errors.length).toBe(0);
	});

	it('should return errors timestamp with isAfter 60 seconds', async () => {
		const now = new Date().setSeconds(new Date().getSeconds() - 120).valueOf();
		const errors = await validateValue(now, 60);
		expect(errors.length).toBe(1);
		expect(errors[0].constraints?.isTimestamp).toEqual('timestamp must be a valid timestamp number');
	});

	it('should validate correctly timestamp with isBefore 60 seconds', async () => {
		const now = new Date().setSeconds(new Date().getSeconds() + 30).valueOf();
		const errors = await validateValue(now, undefined, 60);
		expect(errors.length).toBe(0);
	});

	it('should return errors timestamp with isBefore 60 seconds', async () => {
		const now = new Date().setSeconds(new Date().getSeconds() + 120).valueOf();
		const errors = await validateValue(now, undefined, 60);
		expect(errors.length).toBe(1);
		expect(errors[0].constraints?.isTimestamp).toEqual('timestamp must be a valid timestamp number');
	});

	it('should validate correctly timestamp with isBetween 120 seconds', async () => {
		const errors = await validateValue(new Date().valueOf(), 60, 60);
		expect(errors.length).toBe(0);
	});

	it('should return errors timestamp with isBetween 120 seconds', async () => {
		const now = new Date().setSeconds(new Date().getSeconds() + 120).valueOf();
		const errors = await validateValue(now, 60, 60);
		expect(errors.length).toBe(1);
		expect(errors[0].constraints?.isTimestamp).toEqual('timestamp must be a valid timestamp number');
	});

	it('should return errors with input value as string', async () => {
		// @ts-ignore
		const errors = await validateValue('380501112233');
		expect(errors.length).toBe(1);
	});

	it('should return errors with input value as object', async () => {
		// @ts-ignore
		const errors = await validateValue({ a: 1 });
		expect(errors.length).toBe(1);
		expect(errors[0].constraints?.isTimestamp).toEqual('timestamp must be a valid timestamp number');
	});
});
