import { registerDecorator } from 'class-validator';
import * as moment from 'moment';

/**
 * @param minusSecondsFromNow allowed earlier from now timespan, in seconds
 * @param plusSecondsFromNow allowed later from now timespan, in seconds
 */
export const IsTimestamp =
	(minusSecondsFromNow?: number, plusSecondsFromNow?: number): ((...args: any) => void) =>
	(object: object, propertyName: string): void => {
		registerDecorator({
			name: 'isTimestamp',
			target: object.constructor,
			propertyName,
			validator: {
				defaultMessage: ({ property }: any): string => `${property} must be a valid timestamp number`,
				validate(value: number): boolean {
					try {
						if (value < 1e3) {
							return false;
						}
						const date = moment(new Date(value));
						let valid = date.isValid();
						if (valid && minusSecondsFromNow && plusSecondsFromNow) {
							valid = date.isBetween(moment().subtract(minusSecondsFromNow, 'second'), moment().add(plusSecondsFromNow, 'second'));
						} else if (valid && minusSecondsFromNow && !plusSecondsFromNow) {
							valid = date.isAfter(moment().subtract(minusSecondsFromNow, 'second'));
						} else if (valid && !minusSecondsFromNow && plusSecondsFromNow) {
							valid = date.isBefore(moment().add(plusSecondsFromNow, 'second'));
						}
						return valid;
					} catch (e) {
						return false;
					}
				},
			},
		});
	};
