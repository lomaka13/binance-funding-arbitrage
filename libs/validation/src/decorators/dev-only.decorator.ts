import { UseGuards, applyDecorators } from '@nestjs/common';

import { DevOnlyGuard } from '../guards/dev-only.guard';

export const DevOnly = (): ReturnType<typeof applyDecorators> => applyDecorators(UseGuards(DevOnlyGuard));
