// eslint-disable-next-line max-classes-per-file
import { plainToClass, Type } from 'class-transformer';
import { IsUUID, IsString, IsNotEmpty, IsObject, IsArray, ArrayMinSize, ValidateNested, Length, validate, ValidationError } from 'class-validator';

import { isValidationError, flattenValidationErrors } from '.';

class NodeDto {
	@IsString()
	@Length(3)
	name: string;
}

class BodyDto {
	@IsUUID()
	id: string;

	@IsString()
	@IsNotEmpty()
	name: string;

	@IsArray()
	@ArrayMinSize(2)
	@ValidateNested({ each: true })
	@Type(() => NodeDto)
	@ValidateNested()
	nested: NodeDto[];

	@IsObject()
	@Type(() => NodeDto)
	@ValidateNested()
	parent: NodeDto;
}

const getValidationErrors = async (data: any = {}): Promise<ValidationError[]> => {
	const object = plainToClass(BodyDto, data);
	return validate(object);
};

describe('Common > Validation > Utils', () => {
	describe('isValidationError()', () => {
		it('should check if error is ValidationError', async () => {
			const errors = await getValidationErrors();
			expect(isValidationError(errors[0])).toStrictEqual(true);
		});

		it('should check if errors array is ValidationError', async () => {
			const errors = await getValidationErrors();
			expect(isValidationError(errors)).toStrictEqual(true);
		});

		it('should check if error is not a ValidationError', async () => {
			expect(isValidationError(new Error())).toStrictEqual(false);
		});

		it('should check if list is not a list of ValidationError', async () => {
			const errors = await getValidationErrors();
			expect(isValidationError(errors.push(new Error() as any))).toStrictEqual(false);
		});
	});

	it('flattenValidationErrors() should foramat error messages list', async () => {
		const errors = await getValidationErrors({
			nested: [{}],
			parent: {},
		});
		const messages = flattenValidationErrors(errors);
		expect(messages).toStrictEqual([
			'id must be a UUID',
			'name should not be empty',
			'name must be a string',
			'0.name must be longer than or equal to 3 characters',
			'0.name must be a string',
			'0.name must be longer than or equal to 3 characters',
			'0.name must be a string',
			'0.name must be longer than or equal to 3 characters',
			'0.name must be a string',
			'0.name must be longer than or equal to 3 characters',
			'0.name must be a string',
			'parent.name must be longer than or equal to 3 characters',
			'parent.name must be a string',
		]);
	});
});
