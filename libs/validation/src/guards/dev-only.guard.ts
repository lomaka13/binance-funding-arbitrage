import { CanActivate, ExecutionContext, Inject, Injectable } from '@nestjs/common';

import { ConfigService } from '@binance-test/config';
import { NotExistsException } from '@binance-test/exceptions';

@Injectable()
export class DevOnlyGuard implements CanActivate {
	private readonly isDev: boolean;

	constructor(@Inject(ConfigService) private readonly configService: ConfigService) {
		this.isDev = this.configService.isTest || this.configService.isDev;
	}

	async canActivate(context: ExecutionContext): Promise<boolean> {
		if (this.isDev) {
			return true;
		}
		throw new NotExistsException(`Route not found`);
	}
}
