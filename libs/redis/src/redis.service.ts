import { Injectable, Inject } from '@nestjs/common';
import * as Redis from 'ioredis';
import { v4 } from 'uuid';

import { REDIS_CLIENT } from './redis.constants';
import { RedisClient, RedisClientError } from './redis.provider';

@Injectable()
export class RedisService {
	constructor(@Inject(REDIS_CLIENT) private readonly redisClient: RedisClient) {}

	getClient(name?: string): Redis.Redis {
		if (!name) {
			name = this.redisClient.defaultKey;
		}
		if (!this.redisClient.clients.has(name)) {
			throw new RedisClientError(`client ${name} does not exist`);
		}
		return this.redisClient.clients.get(name)!;
	}

	getClients(): Map<string, Redis.Redis> {
		return this.redisClient.clients;
	}

	async concurrencyWrite<T>(client: Redis.Redis, key: string, cb: (locked: boolean) => T): Promise<T> {
		const token = v4();
		const acquired = await client.set(key, token, 'NX');
		try {
			return cb(acquired !== 'OK');
		} finally {
			await client.del(key);
		}
	}
}
