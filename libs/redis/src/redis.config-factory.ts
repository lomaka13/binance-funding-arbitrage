import * as Redis from 'ioredis';
import { throttle } from 'lodash';

import { ConfigService } from '@binance-test/config';
import { FatalException } from '@binance-test/exceptions';

import { RedisOptions, RedisModuleAsyncOptions, RedisModuleOptions } from './redis.types';
import { RedisVars } from './redis.vars';

export const onClientReady = async (client: Redis.Redis): Promise<void> => {
	client.on(
		'error',
		throttle(
			(error: Error) => {
				throw new FatalException('Redis client error', error);
			},
			5e3,
			{ leading: true },
		),
	);
};

export const createConnectionOptions = (
	configService: ConfigService<keyof typeof RedisVars>,
	keyPrefix?: string,
	name?: string,
): RedisModuleOptions => ({
	host: configService.get('APP_REDIS_HOST'),
	port: configService.getNumber('APP_REDIS_PORT'),
	db: configService.getNumber('APP_REDIS_DB_INDEX', 0),
	password: configService.get('APP_REDIS_PASSWORD'),
	keyPrefix,
	name,
	onClientReady,
	...(configService.getBool('APP_REDIS_SECURE') && {
		tls: {
			...(configService.get('APP_REDIS_CA') && { ca: configService.get('APP_REDIS_CA') }),
		},
	}),
});

export const configFactory =
	(options?: RedisOptions): ((configService: ConfigService<keyof typeof RedisVars>) => ReturnType<typeof createConnectionOptions>) =>
	(configService: ConfigService<keyof typeof RedisVars>): ReturnType<typeof createConnectionOptions> =>
		createConnectionOptions(configService, options?.keyPrefix);

export const createConfigProvider = (options?: RedisOptions): RedisModuleAsyncOptions => ({
	useFactory: options?.useFactory || configFactory(options),
	inject: options?.inject || [ConfigService],
});
