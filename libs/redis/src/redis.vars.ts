import { OPTIONAL } from '@binance-test/config';

export enum RedisVars {
	APP_REDIS_HOST,
	APP_REDIS_PORT,
	APP_REDIS_PASSWORD,
	APP_REDIS_DB_INDEX = OPTIONAL,
	APP_REDIS_SECURE = OPTIONAL,
	APP_REDIS_CA = OPTIONAL,
}

export type RedisVarsType = keyof typeof RedisVars;
