import { Module, DynamicModule } from '@nestjs/common';

import { ConfigService } from '@binance-test/config';

import { createConfigProvider, createConnectionOptions } from './redis.config-factory';
import { RedisCoreModule } from './redis.core-module';
import { RedisModuleOptions, RedisOptions } from './redis.types';

@Module({})
export class RedisModule {
	static register(options?: RedisOptions): DynamicModule {
		return {
			module: RedisModule,
			imports: [RedisCoreModule.forRootAsync(createConfigProvider(options))],
		};
	}

	static registerMulti(options: { name?: string }[]): DynamicModule {
		return {
			module: RedisModule,
			imports: [
				RedisCoreModule.forRootAsync({
					useFactory: (configService: ConfigService): RedisModuleOptions[] => {
						return options.map(({ name }: any) => createConnectionOptions(configService, undefined, name));
					},
					inject: [ConfigService],
				}),
			],
		};
	}
}
