import { ModuleMetadata } from '@nestjs/common/interfaces';
import { Redis, RedisOptions as IoRedisOptions } from 'ioredis';

export interface RedisModuleOptions extends IoRedisOptions {
	name?: string;
	url?: string;

	onClientReady?(client: Redis): Promise<void>;
}

export interface RedisModuleAsyncOptions extends Pick<ModuleMetadata, 'imports'> {
	useFactory?: (...args: any[]) => RedisModuleOptions | RedisModuleOptions[] | Promise<RedisModuleOptions> | Promise<RedisModuleOptions[]>;
	inject?: any[];
}

export interface RedisOptions extends RedisModuleAsyncOptions {
	keyPrefix?: string;
}
