import { BootServiceInterface } from '@binance-test/boot';

export class BootServiceMock implements BootServiceInterface {
	private configService: any;

	private loggerService: any;

	constructor(configService: any, loggerService: any) {
		this.configService = configService;
		this.loggerService = loggerService;
	}

	getConfigService = jest.fn(function (this: BootServiceMock): any {
		return this.configService;
	});

	getLoggerService = jest.fn(function (this: BootServiceMock): any {
		return this.loggerService;
	});

	enableProcessExceptionsHooks = jest.fn();
}
