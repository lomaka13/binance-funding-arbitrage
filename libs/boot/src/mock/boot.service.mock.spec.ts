import { BootServiceInterface } from '@binance-test/boot';

import { BootServiceMock } from './boot.service.mock';

describe('Common > boot > Mock > BootServiceMock', () => {
	const configServiceMock = { configService: true };
	const loggerServiceMock = { loggerService: true };
	let service: BootServiceInterface;

	beforeEach(() => {
		service = new BootServiceMock(configServiceMock, loggerServiceMock);
	});

	afterEach(() => {
		jest.clearAllMocks();
	});

	it('should be defined', () => {
		expect(service).toBeDefined();
	});

	it('should provide predefined configService mock', () => {
		expect(service.getConfigService()).toStrictEqual(configServiceMock);
		expect(service.getConfigService).toBeCalledTimes(1);
	});

	it('should provide predefined loggerService mock', () => {
		expect(service.getLoggerService()).toStrictEqual(loggerServiceMock);
		expect(service.getLoggerService).toBeCalledTimes(1);
	});
});
