import { Module, DynamicModule } from '@nestjs/common';
import { ModuleMetadata } from '@nestjs/common/interfaces';

import { BootService } from './boot.service';

type BootModuleOptions = Pick<ModuleMetadata, 'imports' | 'exports' | 'providers'>;

/**
 * Simple module for app bootstrap script to provde
 * logger and config services 'early access'.
 * Usage example:
 * ```ts
 * const boot = await NestFactory.createApplicationContext(BootModule.register({...});
 * const bootService = boot.get(BootService);
 * const logger = bootService.getLoggerService();
 * const config = bootService.getConfigService();
 * logger.warn(`Starting app: ${config.get('APP_NAME')}`)
 * ```
 */
@Module({
	providers: [BootService],
})
export class BootModule {
	public static register({ imports, exports, providers }: BootModuleOptions): DynamicModule {
		return {
			module: BootModule,
			imports,
			exports,
			providers,
		};
	}
}
