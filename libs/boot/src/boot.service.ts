import { Injectable } from '@nestjs/common';

import { BootServiceInterface } from '@binance-test/boot';
import { ConfigService } from '@binance-test/config';
import { LoggerService } from '@binance-test/logger';

/**
 * Boot serice providing access to config and logger services
 */
@Injectable()
export class BootService implements BootServiceInterface {
	/**
	 * Create boot service
	 */
	constructor(private configService: ConfigService, private loggerService: LoggerService) {}

	/**
	 * Get ready to use config service
	 */
	public getConfigService<T extends string>(): ConfigService<T> {
		return this.configService;
	}

	/**
	 * Get ready to use logger service
	 */
	public getLoggerService(): LoggerService {
		return this.loggerService;
	}

	/**
	 * Enables handlers for exceptional process messages
	 */
	public enableProcessExceptionsHooks(): void {
		const logger = this.loggerService.getLogger('process');

		// Unhandled promise rejection
		process.on('unhandledRejection', (error: any) => {
			logger.fatal({
				type: 'process_unhandled_rejection',
				message: `Process unhandled rejection`,
				error,
			});
			process.exit(1);
		});
		// Uncaught app exception
		process.on('uncaughtException', (error: Error) => {
			logger.fatal({
				type: 'process_uncaught_exception',
				message: `Process uncaught exception`,
				error,
			});
			process.exit(1);
		});
		// Process got warning
		process.on('warning', (error: Error) => {
			logger.warn({
				type: 'process_warning_event',
				message: 'Process warning event',
				error,
			});
		});
		// Process exit
		process.on('exit', (exitCode: number) => {
			logger[exitCode === 0 ? 'info' : 'fatal']({
				type: 'process_exit_event',
				message: `Process exit event (${exitCode})`,
				exitCode,
			});
		});
	}
}
