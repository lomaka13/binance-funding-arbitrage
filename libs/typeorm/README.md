# Typeorm module

Abstract database connection module

### Usage example

Init module with default config factory.

```ts
import { Module } from '@nestjs/common';

import { TypeOrmModule } from '@wlx/common/typeorm';

import { ExampleEntity } from './example.entity';

@Module({
	imports: [
		TypeOrmModule.registerEntities({
			entities: [ExampleEntity]
		}),
	],
})
export class ExampleModule {
}
```

Init module with custom config factory.

```ts
import { Module } from '@nestjs/common';

import { ConfigService } from '@wlx/common/config';
import { ConnectionOptions, TypeOrmModule } from '@wlx/common/typeorm';

import { ExampleEntity } from './example.entity';

@Module({
	imports: [
		TypeOrmModule.registerEntities({
			useFactory: (configService: ConfigService) => ({
				type: configService.get('APP_DB_TYPE', 'postgres') as Extract<ConnectionOptions, 'type'>,
				host: configService.get('APP_DB_HOST'),
				port: configService.getInt('APP_DB_PORT'),
				username: configService.get('APP_DB_USER'),
				password: configService.get('APP_DB_PASSWORD'),
				database: configService.get('APP_DB_NAME'),
				logging: configService.getBoolean('APP_DB_LOGGING', false),
				entities: [ExampleEntity],
			})
			inject: [ConfigService],
		}),
	],
})
export class ExampleModule {
}
```

Inject components

```ts
import { Injectable } from '@nestjs/common';

import { InjectRepository, Repository } from '@wlx/common/typeorm';

import { ExampleEntity } from './example.entity';

@Injectable()
export class AuthService {
	constructor(
		@InjectRepository(ExampleEntity) private readonly exampleRepository: Repository<ExampleEntity>,
	) {
	}
}
```

### Usage with migration generation example

Module import

```ts
import { Module } from '@nestjs/common';
import * as path from 'path';

import { TypeOrmModule } from '@wlx/common/typeorm';

import { ExampleEntity } from './example.entity';

@Module({
	imports: [
		TypeOrmModule.registerEntities({
			entities: [ExampleEntity]
			migrationsDir: path.resolve(__dirname, '../../', 'migrations'),
		}),
	],
})
export class ExampleModule {
}
```

Add to application `tsconfig.migrations.json` with content:
```json
{
  "extends": "../../tsconfig.json",
  "compilerOptions": {
    "declaration": false,
    "outDir": "../../dist/apps/${application}"
  },
  "include": ["migrations/*"],
  "exclude": ["node_modules", "dist", "test", "**/*spec.ts"]
}
```
NOTE! `compilerOptions.outDir` should be explicitly defined for `nest start` to process migrations correctly.

### Migrate from `sync` to `migration`

Now you can remove `APP_DB_SYNC` from variables

Default you used `public` PG schema in our typeorm connection options we used `wlx` schema

Connection module creates `wlx` schema while trying apply migrations(it runs always with `create schema if not exists` query)

* If you want disable generation migration just add environment variable `APP_DB_MIGRATIONS_DISABLE_GENERATE` with value `true`
* If you want disable applying migration just add environment variable `APP_DB_MIGRATIONS_DISABLE_RUN` with value `true`
* If you want create custom migration(without entity changes) - just create new file in migrations folder with name like
`{TIMESTAMP}-{MigrationName}.ts`

How write migration content you can find [HERE](https://typeorm.io/#/migrations/using-migration-api-to-write-migrations)  
