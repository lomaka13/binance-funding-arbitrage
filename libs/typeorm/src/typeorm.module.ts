import { Module, DynamicModule } from '@nestjs/common';
import { TypeOrmModule as NestTypeOrmModule } from '@nestjs/typeorm';

import { createConfigProvider } from './typeorm.config-factory';
import { TYPEORM_CONNECTION_PREFIX } from './typeorm.constants';
import { Connection, ConnectionOptions, createConnection, TypeOrmConfigProviderOptions } from './typeorm.types';

@Module({})
export class TypeOrmModule {
	static registerEntities(options: TypeOrmConfigProviderOptions): DynamicModule {
		const config = createConfigProvider(options);
		return {
			imports: [
				NestTypeOrmModule.forRootAsync({
					useFactory: config.useFactory,
					inject: config.inject,
				}),
				NestTypeOrmModule.forFeature(options.entities),
			],
			module: TypeOrmModule,
			exports: [TypeOrmModule, NestTypeOrmModule],
		};
	}

	static registerManyConnections(options: ConnectionOptions[]): DynamicModule {
		const connections: any[] = options.map((option: ConnectionOptions) => {
			return {
				provide: `${TYPEORM_CONNECTION_PREFIX}${option.name}`,
				useFactory: async (): Promise<Connection> => {
					/**
					 * Cause etlgl service does not have own migrations,
					 * pipeline db doesn't know about schema,
					 * so we need to manually create it.
					 */
					if (option.synchronize) {
						const syncConnection = await createConnection({
							...option,
							synchronize: false,
						});
						if (option.type === 'postgres' && option.schema) {
							await syncConnection.query(`create schema if not exists "${option.schema}"`);
						}
						await syncConnection.close();
					}
					return createConnection(option);
				},
			};
		});
		return {
			providers: connections,
			module: TypeOrmModule,
			exports: connections,
		};
	}
}
