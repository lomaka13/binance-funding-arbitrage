import { Global, Module, DynamicModule } from '@nestjs/common';

import { TypeOrmModule } from './typeorm.module';
import { ConnectionOptions, TypeOrmConfigProviderOptions } from './typeorm.types';

@Global()
@Module({})
export class TypeOrmGlobalModule {
	static registerGlobalEntities(options: TypeOrmConfigProviderOptions): DynamicModule {
		const baseModule = TypeOrmModule.registerEntities(options);
		return {
			imports: [baseModule],
			exports: [baseModule],
			module: TypeOrmGlobalModule,
		};
	}

	static registerManyConnections(options: ConnectionOptions[]): DynamicModule {
		const baseModule = TypeOrmModule.registerManyConnections(options);
		return {
			imports: [baseModule],
			exports: [baseModule],
			module: TypeOrmGlobalModule,
		};
	}
}
