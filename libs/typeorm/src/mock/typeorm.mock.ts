// eslint-disable-next-line max-classes-per-file
import * as faker from 'faker';
import { cloneDeep, difference as diff, pick } from 'lodash';
import { Repository } from 'typeorm';

export { getRepositoryToken, getConnectionToken } from '@nestjs/typeorm';

type MockMethods<T> = {
	// eslint-disable-next-line @typescript-eslint/ban-types
	[K in keyof T as T[K] extends Function ? K : never]: jest.Mock;
};

// type BuilderExcludes<T> = SelectQueryBuilder<T> & InsertQueryBuilder<T> & UpdateQueryBuilder<T>;
// type BuilderMock<T> = MockMethods<Exclude<SelectQueryBuilder<T>, BuilderExcludes<T>>>
// & MockMethods<Exclude<InsertQueryBuilder<T>, BuilderExcludes<T>>>
// & MockMethods<Exclude<UpdateQueryBuilder<T>, BuilderExcludes<T>>>
// & MockMethods<BuilderExcludes<T>>;

export type RepositoryMock<T> = MockMethods<Omit<Repository<T>, 'createQueryBuilder'>> & {
	createQueryBuilder: () => MockMethods<QueryBuilderMock>;
};

/* eslint-disable @typescript-eslint/lines-between-class-members */
class QueryBuilderMock {
	distinct = jest.fn().mockReturnThis();
	select = jest.fn().mockReturnThis();
	delete = jest.fn().mockReturnThis();
	from = jest.fn().mockReturnThis();
	where = jest.fn().mockReturnThis();
	orWhere = jest.fn().mockReturnThis();
	setParameter = jest.fn().mockReturnThis();
	orderBy = jest.fn().mockReturnThis();
	take = jest.fn().mockReturnThis();
	skip = jest.fn().mockReturnThis();
	insert = jest.fn().mockReturnThis();
	into = jest.fn().mockReturnThis();
	values = jest.fn().mockReturnThis();
	onConflict = jest.fn().mockReturnThis();
	getRawOne = jest.fn().mockReturnThis();
	getRawMany = jest.fn().mockReturnThis();
	addSelect = jest.fn().mockReturnThis();
	andWhere = jest.fn().mockReturnThis();
	groupBy = jest.fn().mockReturnThis();
	orIgnore = jest.fn().mockReturnThis();
	getSql = jest.fn().mockReturnThis();
	execute = jest.fn();
	innerJoinAndSelect = jest.fn().mockReturnThis();
	innerJoin = jest.fn().mockReturnThis();
	getOne = jest.fn();
	getMany = jest.fn();
}
/* eslint-enable @typescript-eslint/lines-between-class-members */

export class TypeOrmMock {
	private readonly entity: any;

	public entityInstance: any;

	constructor(entity: any, params: any = {}) {
		this.entity = entity;
		this.entityInstance = new entity();
		Object.keys(params).forEach((key: string) => (this.entityInstance[key] = params[key]));
	}

	factory<T = any>(): RepositoryMock<T> {
		const qb = new QueryBuilderMock();
		const repository = {
			manager: {
				connection: {
					options: {
						schema: 'test',
					},
				},
				query: jest.fn(),
				transaction: jest.fn(async (fn: any): Promise<any> => {
					return await fn(repository);
				}),
			},
			insert: jest.fn(),
			// eslint-disable-next-line @typescript-eslint/ban-types
			create: jest.fn((paramsOrEntity: Function | any, params: object) => {
				const data = new this.entity(params);
				if (typeof paramsOrEntity === 'function') {
					paramsOrEntity = params;
				}
				Object.keys(paramsOrEntity).forEach((key: string) => (data[key] = paramsOrEntity[key]));
				return data;
			}),
			find: jest.fn().mockResolvedValue([this.entityInstance]),
			findByIds: jest.fn().mockResolvedValue([this.entityInstance]),
			findAndCount: jest.fn().mockResolvedValue([[this.entityInstance], 1]),
			update: jest.fn(async (id: unknown, data: unknown) => {
				Object.assign(this.entityInstance, data);
				return { affected: 1 };
			}),
			findOne: jest.fn(async (params: { where: T }) => {
				const pickedByWhere = Object.values(pick(cloneDeep(this.entityInstance), Object.keys(params.where)));
				return diff(Object.values(params.where), pickedByWhere).length > 0 ? undefined : cloneDeep(this.entityInstance);
			}),
			findOneOrFail: jest.fn(async (params: { where: T }) => {
				const pickedByWhere = Object.values(pick(cloneDeep(this.entityInstance), Object.keys(params.where)));
				const notFounded = diff(Object.values(params.where), pickedByWhere).length > 0;
				if (notFounded) {
					throw new Error('Entity not found');
				}
				return cloneDeep(this.entityInstance);
			}),
			findOneBy: jest.fn(async (params: T) => {
				const pickedByWhere = Object.values(pick(cloneDeep(this.entityInstance), Object.keys(params)));
				return diff(Object.values(params), pickedByWhere).length > 0 ? undefined : cloneDeep(this.entityInstance);
			}),
			findOneByOrFail: jest.fn(async (params: T) => {
				const pickedByWhere = Object.values(pick(cloneDeep(this.entityInstance), Object.keys(params)));
				const notFounded = diff(Object.values(params), pickedByWhere).length > 0;
				if (notFounded) {
					throw new Error('Entity not found');
				}
				return cloneDeep(this.entityInstance);
			}),
			remove: jest.fn().mockResolvedValue(true),
			delete: jest.fn().mockResolvedValue(true),
			save: jest.fn(async (data: any) => {
				const entity = new this.entity();
				Object.keys(data).forEach((key: string) => (entity[key] = data[key]));
				entity.id = data.id || faker.random.uuid().toString();
				return entity;
			}),
			createQueryBuilder: jest.fn(() => qb),
			hasId: jest.fn(),
			getId: jest.fn(),
			merge: jest.fn(),
			preload: jest.fn(),
			softRemove: jest.fn(),
			recover: jest.fn(),
			softDelete: jest.fn(),
			restore: jest.fn(),
			count: jest.fn(),
			query: jest.fn(),
			clear: jest.fn(),
			increment: jest.fn(),
			decrement: jest.fn(),
		};

		// @ts-ignore
		return repository;
	}
}
