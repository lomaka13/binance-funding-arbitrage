import { Provider } from '@nestjs/common';
import { DataSource } from 'typeorm';

import { RepositoryMock } from './typeorm.mock';

export const DataSourceMockFactory = (repos: Record<string, RepositoryMock<any>>): Provider => {
	return {
		provide: DataSource,
		useValue: {
			getRepository(entity: any): any {
				const entityClassName = entity.toString();
				for (const name in repos) {
					if (entityClassName.includes(name)) {
						return repos[name];
					}
				}
				throw new Error(`unexpected repo: ${entityClassName}`);
			},
		},
	};
};
