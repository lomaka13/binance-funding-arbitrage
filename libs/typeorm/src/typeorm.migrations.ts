import * as crypto from 'crypto';
import * as pathFs from 'path';

import { omit } from 'lodash';
import { createConnection, ConnectionOptions, Connection, getConnectionManager } from 'typeorm';
import { CommandUtils } from 'typeorm/commands/CommandUtils';
import { Query } from 'typeorm/driver/Query';

import { FatalException } from '@binance-test/exceptions';
import { LoggerService } from '@binance-test/logger';

import { LoggerAdapter } from './typeorm.logger-adapter';

const getTemplate = (timestamp: number, upSqls: string[], downSqls: string[]): string => {
	const migrationName = `CS${crypto.randomBytes(5).toString('hex').toUpperCase()}${timestamp}`;

	return `module.exports = class ${migrationName} {
	name = '${migrationName}';

	async up(queryRunner) {
${upSqls.join(`
`)}
	}

	async down(queryRunner) {
${downSqls.join(`
`)}
	}
};
`;
};

const getDefaultConnection = (): Connection | undefined => {
	const defaultConnection = getConnectionManager().get('default');
	if (defaultConnection.isConnected) {
		return defaultConnection;
	}
};

export const generateMigrations = async (loggerService: LoggerService, connectionOptions: ConnectionOptions): Promise<void> => {
	let connection: Connection;
	try {
		connection = await createConnection({
			...connectionOptions,
			synchronize: false,
			migrationsRun: false,
			dropSchema: false,
			logging: true,
			logger: new LoggerAdapter(loggerService.getLogger('typeorm-generate-migration'), ['warn', 'error']),
		});
	} catch (error: any) {
		const defaultConnection = getDefaultConnection();
		if (defaultConnection) {
			connection = defaultConnection;
		} else {
			const logger = loggerService.getLogger('typeorm-generate-migration');
			logger.error({
				type: 'migration_connection_error',
				message: `Migration connection error: ${error.message}`,
				error,
				connectionOptions: omit(connectionOptions, ['username', 'password']),
			});
			throw new FatalException(error.message);
		}
	}
	const sqlInMemory = await connection.driver.createSchemaBuilder().log();
	const directory = (connectionOptions as any).cli!.migrationsDir;
	const timestamp = new Date().getTime();
	const filename = `${timestamp}.js`;

	const upSqls: string[] = sqlInMemory.upQueries
		.filter((upQuery: Query) => !upQuery.query.toLowerCase().includes('create extension'))
		.map(
			(upQuery: Query) =>
				// eslint-disable-next-line max-len
				'	await queryRunner.query(`' + upQuery.query.replace(/`/g, '\\`') + '`, ' + JSON.stringify(upQuery.parameters) + ');',
		);
	const downSqls: string[] = sqlInMemory.downQueries.map(
		(downQuery: Query) =>
			// eslint-disable-next-line max-len
			'		await queryRunner.query(`' + downQuery.query.replace(/`/g, '\\`') + '`, ' + JSON.stringify(downQuery.parameters) + ');',
	);
	if (upSqls.length) {
		downSqls.reverse();
		const fileContent = getTemplate(timestamp, upSqls, downSqls);
		const path = pathFs.join(directory as string, filename);
		await CommandUtils.createFile(path, fileContent);
		// const distPath = (connectionOptions.migrations![0] as string)
		// 	.replace('/*.js', '')
		// 	.replace('/*.ts', '');
		// if ((directory as string) !== distPath) {
		// 	const srcPath = (directory as string).split('/');
		// 	await execAsync(`tsc -p apps/${srcPath[srcPath.length - 2]}/tsconfig.migrations.json`, {
		// 		cwd: process.cwd(),
		// 	});
		// }
	}
	return connection.close();
};

export const applyMigrations = async (loggerService: LoggerService, connectionOptions: ConnectionOptions): Promise<void> => {
	let connection: Connection;
	try {
		connection = await createConnection({
			...connectionOptions,
			synchronize: false,
			migrationsRun: false,
			dropSchema: false,
			logging: true,
			logger: new LoggerAdapter(loggerService.getLogger('typeorm-apply-migration'), ['query', 'schema', 'warn', 'error']),
		});
	} catch (error: any) {
		const defaultConnection = getDefaultConnection();
		if (defaultConnection) {
			connection = defaultConnection;
		} else {
			const log = loggerService.getLogger('typeorm-apply-migration');
			log.error({
				type: 'migration_connection_error',
				message: `Migration connection error: ${error.message}`,
				error,
				connectionOptions: omit(connectionOptions, ['username', 'password']),
			});
			throw new FatalException(error.message);
		}
	}
	if ('schema' in connectionOptions) {
		await connection.query(`create schema if not exists "${connectionOptions.schema}"`);
	}
	await connection.runMigrations({ transaction: 'all' });
	return connection.close();
};

export const rollbackMigrations = async (loggerService: LoggerService, connectionOptions: ConnectionOptions, count: number): Promise<void> => {
	let connection: Connection;
	try {
		connection = await createConnection({
			...connectionOptions,
			synchronize: false,
			migrationsRun: false,
			dropSchema: false,
			logging: true,
			logger: new LoggerAdapter(loggerService.getLogger('typeorm-apply-migration'), ['query', 'schema', 'warn', 'error']),
		});
	} catch (error: any) {
		const defaultConnection = getDefaultConnection();
		if (defaultConnection) {
			connection = defaultConnection;
		} else {
			const log = loggerService.getLogger('typeorm-apply-migration');
			log.error({
				type: 'migration_connection_error',
				message: `Migration connection error: ${error.message}`,
				error,
				connectionOptions: omit(connectionOptions, ['username', 'password']),
			});
			throw new FatalException(error.message);
		}
	}
	if ('schema' in connectionOptions) {
		await connection.query(`create schema if not exists "${connectionOptions.schema}"`);
	}
	for (let i = 0; i < count; i++) {
		await connection.undoLastMigration({ transaction: 'all' });
	}
	return connection.close();
};
