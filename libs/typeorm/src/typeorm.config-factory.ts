import * as path from 'path';

import { FactoryProvider } from '@nestjs/common/interfaces';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { each as eachPromise } from 'bluebird';
import { ConnectionOptions, EntitySchema } from 'typeorm';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';

import { ConfigService } from '@binance-test/config';
import { LoggerService } from '@binance-test/logger';

import { TYPEORM_CONFIG_OPTIONS, TYPEORM_SCHEMA } from './typeorm.constants';
import { LoggerAdapter } from './typeorm.logger-adapter';
import { applyMigrations, generateMigrations, rollbackMigrations } from './typeorm.migrations';
import { TypeOrmConfigProviderOptions } from './typeorm.types';
import { TypeOrmVarsType } from './typeorm.vars';

type P = Promise<any | void>;
type FnP = () => P;

type FactoryFunc = (configService: ConfigService<TypeOrmVarsType>, loggerService: LoggerService) => Promise<TypeOrmModuleOptions>;

const queue: FnP[] = [];
let promise: Promise<any>;
const processQueue = async (): Promise<any> => {
	if (promise) {
		await promise;
	}
	return (promise = eachPromise(queue.splice(0, queue.length), (fn: () => unknown) => fn()));
};

export const createConnectionOptions = (
	configService: ConfigService<TypeOrmVarsType>,
	loggerService: LoggerService,
	entities?: (typeof Function | string | EntitySchema<any>)[],
): TypeOrmModuleOptions => {
	const logging = configService.getBoolean('APP_DB_LOGGING', false);
	return {
		schema: TYPEORM_SCHEMA,
		type: configService.get('APP_DB_TYPE', 'postgres') as Extract<ConnectionOptions, 'type'>,
		host: configService.get('APP_DB_HOST'),
		port: configService.getInt('APP_DB_PORT'),
		username: configService.get('APP_DB_USER'),
		password: configService.get('APP_DB_PASSWORD'),
		database: configService.get('APP_DB_NAME'),
		logging,
		logger: logging ? new LoggerAdapter(loggerService.getLogger('typeorm'), 'all') : undefined,
		synchronize: false,
		entities,
		debug: true,
		autoLoadEntities: true,
		keepConnectionAlive: true,
		retryAttempts: 3,
		relationLoadStrategy: 'query',
		maxQueryExecutionTime: 1000,
		extra: {
			applicationName: configService.get('APP_NAME'),
			...(!configService.isTest && {
				max: configService.getNumber('APP_DB_MAX_CONNECTION_POOL', 10),
				idleTimeoutMillis: configService.getNumber('APP_DB_MAX_CONNECTION_IDLE_TIMEOUT', 30000),
				connectionTimeoutMillis: configService.getNumber('APP_DB_MAX_CONNECTION_IDLE_TIMEOUT', 2000),
			}),
		},
	};
};

const configFactory =
	(options: TypeOrmConfigProviderOptions): FactoryFunc =>
	async (configService: ConfigService<TypeOrmVarsType>, loggerService: LoggerService): Promise<TypeOrmModuleOptions> => {
		let distDir: string | undefined = 'dist';
		let cliMigrationsDir: string | undefined;
		if (!configService.isLocal) {
			distDir = path.resolve(__dirname);
			cliMigrationsDir = path.resolve(__dirname, 'migrations');
		}
		const connectionOptions = createConnectionOptions(configService, loggerService, options.entities);
		const builderOptions = {
			...connectionOptions,
			cli: { migrationsDir: cliMigrationsDir || options.migrationsDir },
			migrations: [
				`${distDir}/migrations/*.js`, // path for webpack
				`./migrations/*.js`, // path for webpack
				`${options.migrationsDir}/*.js`, // real migrations path js
			],
		};
		const migrationRuns = !configService.getBoolean('APP_DB_MIGRATIONS_DISABLE_RUN', false);
		if (!configService.getBoolean('APP_DB_MIGRATIONS_DISABLE_GENERATE', true) && options.migrationsDir) {
			queue.push(async () => generateMigrations(loggerService, builderOptions as ConnectionOptions));
		}
		if (migrationRuns && options.migrationsDir) {
			queue.push(async () => applyMigrations(loggerService, builderOptions as ConnectionOptions));
		}
		if (!migrationRuns && options.migrationsDir && configService.getBoolean('APP_DB_MIGRATIONS_ROLLBACK_RUN', false)) {
			queue.push(async () =>
				rollbackMigrations(loggerService, builderOptions as ConnectionOptions, configService.getNumber('APP_DB_MIGRATIONS_ROLLBACK_COUNT', 1)),
			);
		}

		await processQueue();

		return connectionOptions;
	};

export const createConfigProvider = (options: TypeOrmConfigProviderOptions): FactoryProvider => {
	return {
		provide: options.provide || TYPEORM_CONFIG_OPTIONS,
		useFactory: options.useFactory || configFactory(options),
		inject: options.inject || [ConfigService, LoggerService],
	};
};

export const createPgConnectionString = (config: PostgresConnectionOptions): string => {
	return `postgresql://${config.username}:${config.password}@${config.host}:${config.port}/${config.database}?currentSchema=${config.schema}`;
};
