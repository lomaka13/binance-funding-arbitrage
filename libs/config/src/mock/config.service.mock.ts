import { URL } from 'url';

import { ConfigEnv } from '@binance-test/config';

interface Config {
	[name: string]: any;
}
export interface EnvConfig {
	env?: ConfigEnv;
	isLocal?: boolean;
	isTest?: boolean;
	isDev?: boolean;
	isUat?: boolean;
	isProd?: boolean;
}

export const createConfigServiceMock = <T = ConfigServiceMock>(config: Config = {}, envConfig?: EnvConfig): T =>
	new ConfigServiceMock(config, envConfig) as any;

/**
 * Config service mock
 */
export class ConfigServiceMock {
	private config: Config = {};

	public env: ConfigEnv = 'test';

	public isLocal = false;

	public isTest = true;

	public isDev = false;

	public isUat = false;

	public isProd = false;

	constructor(config: Config, envConfig?: EnvConfig) {
		Object.assign(this.config, config);
		Object.assign(this, envConfig);
	}

	public set = jest.fn(function (this: ConfigServiceMock, key: string, value: any): ConfigServiceMock {
		this.config[key] = value;
		return this;
	});

	public has = jest.fn((key: string) => {
		return this.config.hasOwnProperty(key);
	});

	public get = jest.fn((key: string, defaultValue?: string) => {
		return this.config[key] || defaultValue;
	});

	public getNumber = jest.fn((key: string, defaultValue?: number) => {
		return this.config[key] || defaultValue;
	});

	public getInt = jest.fn((key: string, defaultValue?: number) => {
		return this.config[key] || defaultValue;
	});

	public getFloat = jest.fn((key: string, defaultValue?: number) => {
		return this.config[key] || defaultValue;
	});

	public getBool = jest.fn((key: string, defaultValue?: boolean) => {
		return Object.keys(this.config).includes(key) ? this.config[key] : defaultValue;
	});

	public getBoolean = jest.fn((key: string, defaultValue?: boolean) => {
		return Object.keys(this.config).includes(key) ? this.config[key] : defaultValue;
	});

	public getPeriodSeconds = jest.fn((key: string, defaultValue?: string) => {
		return this.config[key] || defaultValue;
	});

	public getPeriodMs = jest.fn((key: string, defaultValue?: string) => {
		return this.config[key] || defaultValue;
	});

	public getUrl = jest.fn((key: string, defaultValue?: string) => {
		return new URL(this.config[key] || defaultValue);
	});

	public getJson = jest.fn((key: string, defaultValue?: string) => {
		return this.config[key] || defaultValue;
	});

	public getEnumValue = jest.fn((key: string, values: string[], defaultValue?: string) => {
		return this.config[key] || defaultValue;
	});

	public ensureConfig = jest.fn();
}
