import { Test, TestingModule } from '@nestjs/testing';

import { ConfigService } from '@binance-test/config';

import { ConfigModuleMock } from './config.module.mock';

describe('Common > config > ConfigModuleMock', () => {
	let service: ConfigService;
	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			imports: [
				ConfigModuleMock.register({
					APP_TEST_STR: 'config-mock-string',
					APP_TEST_NUMBER: 123,
				}),
			],
		}).compile();

		service = module.get(ConfigService);
	});

	afterEach(() => {
		jest.clearAllMocks();
	});

	it('should be defined', () => {
		expect(service).toBeDefined();
	});

	it('should provide has() method, case 1', () => {
		expect(service.has('APP_TEST_STR')).toStrictEqual(true);
		expect(service.has).toBeCalledTimes(1);
		expect(service.has).toBeCalledWith('APP_TEST_STR');
	});

	it('should provide has() method, case 2', () => {
		expect(service.has('APP__')).toStrictEqual(false);
		expect(service.has).toBeCalledTimes(1);
		expect(service.has).toBeCalledWith('APP__');
	});

	it('should provide set() method', () => {
		expect(service.set('APP__', 'QWERTY'));
		expect(service.set).toBeCalledTimes(1);
		expect(service.has('APP__')).toStrictEqual(true);
	});

	it('should provide get() method', () => {
		expect(service.get('APP_TEST_STR')).toStrictEqual('config-mock-string');
		expect(service.get('APP_TEST_NUMBER')).toStrictEqual(123);
	});
});
