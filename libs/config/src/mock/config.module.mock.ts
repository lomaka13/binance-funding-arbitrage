// eslint-disable-next-line max-classes-per-file
import { DynamicModule, Global, Module } from '@nestjs/common';

import { ConfigService } from '@binance-test/config';

import { createConfigServiceMock, EnvConfig } from './config.service.mock';

@Module({})
export class ConfigModuleMock {
	static register(env: { [name: string]: any } = {}, envConfig: EnvConfig = {}): DynamicModule {
		return {
			module: ConfigModuleMock,
			providers: [
				{
					provide: ConfigService,
					useValue: createConfigServiceMock(env, envConfig),
				},
			],
			exports: [ConfigService],
		};
	}
}

@Global()
@Module({})
export class ConfigGlobalModuleMock extends ConfigModuleMock {
	static register(env: { [name: string]: any } = {}, envConfig: EnvConfig = {}): DynamicModule {
		return {
			...super.register(env, envConfig),
			module: ConfigGlobalModuleMock,
		};
	}
}
