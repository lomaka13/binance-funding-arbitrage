export const json = (payload: string): any => JSON.parse(payload);

export const base64 = (payload: string): string => {
	return Buffer.from(payload, 'base64').toString('utf8');
};
