import { DynamicModule, Global, Module } from '@nestjs/common';

import { BootServiceInterface } from '@binance-test/boot';

import { ConfigService } from '.';

@Global()
@Module({})
export class ConfigGlobalModule {
	static forRoot(bootService: BootServiceInterface): DynamicModule {
		return {
			module: ConfigGlobalModule,
			providers: [
				{
					provide: ConfigService,
					useValue: bootService.getConfigService(),
				},
			],
			exports: [ConfigService],
		};
	}
}
