import { URL } from 'url';

import { Test, TestingModule } from '@nestjs/testing';
import * as moment from 'moment';

import { AppCommonVarsType } from '@binance-test/app';
import { OPTIONAL } from '@binance-test/config';
import { FatalException } from '@binance-test/exceptions';

import { ConfigModule } from './config.module';
import { ConfigService } from './config.service';
import { TestDto } from './fixtures';

const commonVars: { [P in AppCommonVarsType]: string } = {
	NODE_ENV: 'test',
	APP_NAME: 'common',
	APP_VERSION: '0.0.1-test',
	APP_LOG_LEVEL: 'all',
	APP_PORT: '3000',
	APP_HOST: 'localhost',
};

describe('Config > ConfigService', () => {
	const APP_ENV = 'APP_ENV';
	const environments = ['testing', 'production'];

	const factory = async (options: any = {}): Promise<ConfigService> => {
		const module: TestingModule = await Test.createTestingModule({
			imports: [
				ConfigModule.register({
					variables: {
						LIB_NAME: 'config',
						TEST_NUM: 123,
						...options,
					},
				}),
			],
		}).compile();

		return module.get(ConfigService);
	};

	it('should be defined', async () => {
		expect(await factory()).toBeDefined();
	});
	it('should be possible to setting environment variables', async () => {
		const env = 'testing';
		const service = await factory();

		service.set(APP_ENV, env);

		expect(service.get(APP_ENV)).toEqual(env);
	});

	it('should possible to rewrite environment variable', async () => {
		const service = await factory();
		environments.forEach((env: string) => {
			service.set(APP_ENV, env);
		});

		expect(service.get(APP_ENV)).toEqual(environments[1]);
	});

	it('should possible to ensure if variable exists into config', async () => {
		const service = await factory();
		service.set(APP_ENV, environments[0]);

		expect(service.has(APP_ENV)).toBeTruthy();
	});

	it('should return undefined for non-existing variable', async () => {
		const service = await factory();
		expect(service.get('non-existent')).toBeUndefined();
	});

	it('should return default for non-existing variable', async () => {
		const service = await factory();
		expect(service.get('non-existent', 'default')).toEqual('default');
	});

	it('should return number', async () => {
		const service = await factory();
		service.set('number', '123');
		expect(service.getNumber('number')).toStrictEqual(123);
	});

	it('should return NaN for failed number conversion', async () => {
		const service = await factory();
		service.set('number', 'non-number');
		expect(service.getNumber('number')).toBeNaN();
	});

	it('should return default for non-existing number variable', async () => {
		const service = await factory();
		expect(service.getNumber('number', 123)).toStrictEqual(123);
	});

	it('should return parsed int', async () => {
		const service = await factory();
		service.set('parse-int', ' 123.12 extra');
		expect(service.getInt('parse-int')).toStrictEqual(123);
	});

	it('should return NaN for failed int parse', async () => {
		const service = await factory();
		service.set('parse-int', 'non-int');
		expect(service.getInt('parse-int')).toBeNaN();
	});

	it('should return default for non-existing int variable', async () => {
		const service = await factory();
		expect(service.getInt('parse-int', 123)).toStrictEqual(123);
	});

	it('should return parsed float', async () => {
		const service = await factory();
		service.set('parse-float', ' 11.12 extra');
		expect(service.getFloat('parse-float')).toStrictEqual(11.12);
	});

	it('should return NaN for failed float parse', async () => {
		const service = await factory();
		service.set('parse-float', 'non-float');
		expect(service.getFloat('parse-float')).toBeNaN();
	});

	it('should return default for non-existing float variable', async () => {
		const service = await factory();
		expect(service.getFloat('parse-float', 11.12)).toStrictEqual(11.12);
	});

	it('should return converted to bool: true', async () => {
		const service = await factory();
		service.set('bool-true', 'TrUe');
		expect(service.getBool('bool-true')).toStrictEqual(true);
	});

	it('should return converted to bool: false', async () => {
		const service = await factory();
		service.set('bool-false', 'FaLse');
		expect(service.getBool('bool-false')).toStrictEqual(false);
	});

	it('should return undefined for failed bool parse', async () => {
		const service = await factory();
		service.set('bool', 'non-bool');
		expect(service.getBool('bool')).toBeUndefined();
	});

	it('should return default for non-existing bool variable', async () => {
		const service = await factory();
		expect(service.getBool('bool', true)).toStrictEqual(true);
	});

	it('should decode base64 values', async () => {
		const val = 'TEST';
		const encVal = Buffer.from(val).toString('base64');
		const service = await factory();
		service.set(APP_ENV, `base64:${encVal}`);

		expect(service.get(APP_ENV)).toStrictEqual(val);
	});

	it('should parse json values', async () => {
		const val = {
			foo: 'bar',
		};
		const encVal = JSON.stringify(val);
		const service = await factory();
		service.set(APP_ENV, `json:${encVal}`);

		expect(service.get(APP_ENV)).toStrictEqual(val);
	});

	it('should provide basic underlying functionality', async () => {
		const service = await factory();
		expect(service.has('TEST_VAL')).toStrictEqual(false);
		expect(service.has('LIB_NAME')).toStrictEqual(true);
		expect(service.get('LIB_NAME')).toStrictEqual('config');
		expect(service.get('TEST_NUM')).toStrictEqual(123);
		expect(service.set('TEST_VAL', 'foo').get('TEST_VAL')).toStrictEqual('foo');
	});

	it('should provide normalized env value with start&end whitespace chars', async () => {
		expect((await factory({ TEST_WHITESPACES: 'test\n' })).get('TEST_WHITESPACES')).toStrictEqual('test');
		expect((await factory({ TEST_WHITESPACES: ' test\t' })).get('TEST_WHITESPACES')).toStrictEqual('test');
		expect((await factory({ TEST_WHITESPACES: 'test\r' })).get('TEST_WHITESPACES')).toStrictEqual('test');
	});

	it('should provide normalized DEV env value', async () => {
		expect((await factory({ NODE_ENV: 'dev' })).env).toStrictEqual('dev');
		expect((await factory({ NODE_ENV: 'development' })).env).toStrictEqual('dev');
		expect((await factory({ NODE_ENV: 'DEV' })).env).toStrictEqual('dev');
	});

	it('should provide normalized UAT env value', async () => {
		expect((await factory({ NODE_ENV: 'uat' })).env).toStrictEqual('uat');
		expect((await factory({ NODE_ENV: 'UAT' })).env).toStrictEqual('uat');
	});

	it('should provide normalized PROD env value', async () => {
		expect((await factory({ NODE_ENV: 'prod' })).env).toStrictEqual('prod');
		expect((await factory({ NODE_ENV: 'production' })).env).toStrictEqual('prod');
		expect((await factory({ NODE_ENV: 'PROD' })).env).toStrictEqual('prod');
	});

	it('should set evn to PROD by default for security reasons', async () => {
		expect((await factory({ NODE_ENV: '' })).env).toStrictEqual('prod');
		expect((await factory({ NODE_ENV: undefined })).env).toStrictEqual('prod');
		expect((await factory({ NODE_ENV: 'unknown' })).env).toStrictEqual('prod');
	});

	it('should set boolean flags by env', async () => {
		expect((await factory({ NODE_ENV: 'dev' })).isDev).toStrictEqual(true);
		expect((await factory({ NODE_ENV: 'uat' })).isUat).toStrictEqual(true);
		expect((await factory({ NODE_ENV: 'prod' })).isProd).toStrictEqual(true);
	});

	it('should ensure config variables are set', async () => {
		const service = await factory(commonVars);
		enum VARS {
			LIB_NAME,
			TEST_NUM,
		}
		expect(service.ensureConfig.bind(service, VARS)).not.toThrow();
	});

	it('should throw if passed config varaiables are missing', async () => {
		const service = await factory(commonVars);
		enum VARS {
			MISSING_VAR,
		}
		expect(service.ensureConfig.bind(service, VARS)).toThrowError(/^Config variables missed/);
	});

	it('should not throw if missing config varaiables are optional', async () => {
		const service = await factory(commonVars);
		enum VARS {
			MISSING_VAR1 = OPTIONAL,
			MISSING_VAR2 = OPTIONAL,
		}
		expect(service.ensureConfig.bind(service, VARS)).not.toThrow();
	});

	describe('duration functionality', () => {
		it('should get period object', async () => {
			const day = moment.duration(1, 'day');
			expect((await factory({ TEST_PERIOD: '1 d' })).getMomentDuration('TEST_PERIOD')).toStrictEqual(day);
		});

		it('should throw doesn`t match regular expression', async () => {
			expect.assertions(2);
			try {
				(await factory({ TEST_PERIOD: '1 invalidPeriod' })).getMomentDuration('TEST_PERIOD');
			} catch (error: any) {
				expect(error).toBeInstanceOf(FatalException);
				// eslint-disable-next-line max-len
				expect(error.message).toStrictEqual(
					`TEST_PERIOD doesn't match regular expression /(\\d+)\\s(year|years|y|month|months|M|week|weeks|w|day|days|d|hour|hours|h|minute|minutes|m|second|seconds|s|millisecond|milliseconds|ms)/`,
				);
			}
		});

		it('should throw has empty value', async () => {
			expect.assertions(2);
			try {
				(await factory()).getMomentDuration('TEST_PERIOD');
			} catch (error: any) {
				expect(error).toBeInstanceOf(FatalException);
				expect(error.message).toStrictEqual('TEST_PERIOD has empty value');
			}
		});

		it('should get period in seconds', async () => {
			const day = moment.duration(1, 'day').asSeconds();
			const month = moment.duration(1, 'month').asSeconds();
			expect((await factory({ TEST_PERIOD: '1 d' })).getPeriodSeconds('TEST_PERIOD')).toStrictEqual(day);
			expect((await factory({ TEST_PERIOD: '1' })).getPeriodSeconds('TEST_PERIOD')).toStrictEqual(day);
			expect((await factory({ TEST_PERIOD: 1 })).getPeriodSeconds('TEST_PERIOD')).toStrictEqual(day);
			expect((await factory({ TEST_PERIOD: '1 month' })).getPeriodSeconds('TEST_PERIOD')).toStrictEqual(month);
			expect((await factory({ TEST_PERIOD: '-1 month' })).getPeriodSeconds('TEST_PERIOD')).toStrictEqual(-month);
		});

		it('should get period in millisecond', async () => {
			const day = moment.duration(1, 'day').asMilliseconds();
			const month = moment.duration(1, 'month').asMilliseconds();
			expect((await factory({ TEST_PERIOD: '1 d' })).getPeriodMs('TEST_PERIOD')).toStrictEqual(day);
			expect((await factory({ TEST_PERIOD: '1' })).getPeriodMs('TEST_PERIOD')).toStrictEqual(day);
			expect((await factory({ TEST_PERIOD: 1 })).getPeriodMs('TEST_PERIOD')).toStrictEqual(day);
			expect((await factory({ TEST_PERIOD: '1 month' })).getPeriodMs('TEST_PERIOD')).toStrictEqual(month);
			expect((await factory({ TEST_PERIOD: '-1 month' })).getPeriodMs('TEST_PERIOD')).toStrictEqual(-month);
		});
	});

	describe('getUrl functionality', () => {
		it('should provide normalized env URL value', async () => {
			const url = new URL('http://test');
			expect((await factory({ TEST_URL: 'http://test' })).getUrl('TEST_URL')).toStrictEqual(url);
		});

		it('should provide normalized env URL value from default value', async () => {
			const url = new URL('http://test');
			expect((await factory()).getUrl('TEST_URL', 'http://test')).toStrictEqual(url);
		});

		it('should throw with invalid url string', async () => {
			expect.assertions(2);
			try {
				expect((await factory({ TEST_URL: 'test.com' })).getUrl('TEST_URL'));
			} catch (error: any) {
				expect(error.code).toStrictEqual('ERR_INVALID_URL');
				expect(error.message).toStrictEqual('Invalid URL');
			}
		});

		it('should throw with empty url string', async () => {
			expect.assertions(2);
			try {
				(await factory()).getUrl('TEST_URL');
			} catch (error: any) {
				expect(error.code).toStrictEqual('ERR_INVALID_URL');
				expect(error.message).toStrictEqual('Invalid URL');
			}
		});
	});

	describe('getJson functionality', () => {
		it('should provide normalized env object from base64 encoded JSON value', async () => {
			const object = { test: 1, a: { b: 2 } };
			const base64 = `base64:${Buffer.from(JSON.stringify(object)).toString('base64')}`;
			expect((await factory({ TEST_JSON: base64 })).getJson('TEST_JSON')).toStrictEqual(object);
		});

		it('should provide normalized env object from JSON value', async () => {
			const object = { test: 1, a: { b: 2 } };
			const json = JSON.stringify(object);
			expect((await factory({ TEST_JSON: json })).getJson('TEST_JSON')).toStrictEqual(object);
		});

		it('should provide normalized env object from base64 encoded JSON value with validation by TestDto', async () => {
			const object = { test: 1, a: { b: 2 } };
			const base64 = `base64:${Buffer.from(JSON.stringify(object)).toString('base64')}`;
			expect((await factory({ TEST_JSON: base64 })).getJson('TEST_JSON', {}, TestDto)).toStrictEqual(object);
		});

		it('should validate as array', async () => {
			const object = [
				{ test: 1, a: { b: 2 } },
				{ test: 1, a: { b: 2 } },
			];
			const base64 = `base64:${Buffer.from(JSON.stringify(object)).toString('base64')}`;
			const service = await factory({ TEST_JSON: base64 });
			expect(service.getJson('TEST_JSON', [], TestDto, true)).toStrictEqual(object);
		});

		it('should validate if array is array', async () => {
			const object = {};
			const base64 = `base64:${Buffer.from(JSON.stringify(object)).toString('base64')}`;
			const service = await factory({ TEST_JSON: base64 });
			expect(() => service.getJson('TEST_JSON', [], TestDto, true)).toThrow('Array expected in TEST_JSON');
		});

		it('should throw invalid JSON from base64 string', async () => {
			expect.assertions(2);
			const base64 = `base64:${Buffer.from('').toString('base64')}`;
			try {
				(await factory({ TEST_JSON: base64 })).getJson('TEST_JSON');
			} catch (error: any) {
				expect(error).toBeInstanceOf(Error);
				expect(error.message).toStrictEqual('Unexpected end of JSON input');
			}
		});

		it('should throw validation from base64 string', async () => {
			expect.assertions(3);
			const base64 = `base64:${Buffer.from(JSON.stringify({ test: '1' })).toString('base64')}`;
			try {
				(await factory({ TEST_JSON: base64 })).getJson('TEST_JSON', {}, TestDto);
			} catch (error: any) {
				expect(error).toBeInstanceOf(FatalException);
				expect(error.originalError).toHaveLength(2);
				expect(error.message).toStrictEqual('TEST_JSON has validation errors by TestDto');
			}
		});
	});

	describe('getEnumValue functionality', () => {
		enum TestEnum {
			A = 'a',
			B = 'b',
		}

		it('should get value from enum', async () => {
			const result = (await factory({ TEST_ENUM_VALUE: 'a' })).getEnumValue('TEST_ENUM_VALUE', Object.values(TestEnum));
			expect(result).toStrictEqual(TestEnum.A);
		});

		it('should get default value from enum', async () => {
			const result = (await factory()).getEnumValue('TEST_ENUM_VALUE', Object.values(TestEnum), TestEnum.A);
			expect(result).toStrictEqual(TestEnum.A);
		});

		it('should throw error default value not valid', async () => {
			const result = await factory({ TEST_ENUM_VALUE: 'otherValue' });
			expect(result.getEnumValue.bind(result, 'TEST_ENUM_VALUE', Object.values(TestEnum))).toThrowError(/otherValue not includes in a, b/);
		});
	});
});
