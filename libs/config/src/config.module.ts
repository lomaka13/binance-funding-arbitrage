import { DynamicModule, Module } from '@nestjs/common';

import { createConfigProviders } from './config.providers';
import { ConfigService } from './config.service';

interface ConfigModuleOptions {
	variables?: { [key: string]: string };
	filePath?: string;
}

/**
 * Config module provides config service
 */
@Module({
	providers: [ConfigService],
	exports: [ConfigService],
})
export class ConfigModule {
	/**
	 * Register config options source, use environment vars by default
	 */
	public static register({ variables, filePath }: ConfigModuleOptions = {}): DynamicModule {
		// eslint-disable-next-line security/detect-non-literal-require
		const options: any = filePath ? require(filePath) : variables || process.env;
		const providers = createConfigProviders({ options });

		return {
			module: ConfigModule,
			providers,
		};
	}
}
