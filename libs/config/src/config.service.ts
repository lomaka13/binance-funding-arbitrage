import { URL } from 'url';

import { Injectable, Inject, Type } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validateSync } from 'class-validator';
import { isEmpty } from 'lodash';
import * as moment from 'moment';

import { FatalException } from '@binance-test/exceptions';

import { CONFIG_OPTIONS, CONFIG_HANDLERS } from './config.constants';
import { ConfigEnv, OPTIONAL, Options, Handlers } from './config.types';

const envMap: { [val: string]: ConfigEnv } = {
	local: 'local',
	test: 'test',
	dev: 'dev',
	development: 'dev',
	uat: 'uat',
	prod: 'prod',
	production: 'prod',
};

const momentDurationInputArray: string[] = [
	'year',
	'years',
	'y',
	'month',
	'months',
	'M',
	'week',
	'weeks',
	'w',
	'day',
	'days',
	'd',
	'hour',
	'hours',
	'h',
	'minute',
	'minutes',
	'm',
	'second',
	'seconds',
	's',
	'millisecond',
	'milliseconds',
	'ms',
];

/**
 * Config helper service
 */
@Injectable()
export class ConfigService<T extends string = string> {
	private readonly durationMatchRegex: RegExp = new RegExp(`(\\d+)\\s(${momentDurationInputArray.join('|')})`);

	/**
	 * Creates an instance of ConfigService.
	 * @param {Options} [config={}]
	 * @param {Handlers} [handlers={}]
	 * @memberof ConfigService
	 */
	constructor(@Inject(CONFIG_OPTIONS) private readonly config: Options = {}, @Inject(CONFIG_HANDLERS) private readonly handlers: Handlers = {}) {}

	/**
	 * Normalized env name
	 */
	public readonly env: ConfigEnv = envMap[String(this.get('NODE_ENV' as any) || 'prod').toLowerCase()] || 'prod';

	/**
	 * Is LOCAL env
	 */
	public readonly isLocal = this.env === 'local';

	/**
	 * Is TEST env
	 */
	public readonly isTest = this.env === 'test';

	/**
	 * Is DEV env
	 */
	public readonly isDev = this.isLocal || this.env === 'dev';

	/**
	 * Is UAT env
	 */
	public readonly isUat = this.env === 'uat';

	/**
	 * Is PROD env
	 */
	public readonly isProd = this.env === 'prod';

	/**
	 * Set variable to config
	 *
	 * @param {string} key
	 * @param {string} value
	 * @returns {ConfigService<T>}
	 * @memberof ConfigService
	 */
	public set(key: T, value: string): ConfigService<T> {
		this.config[key] = value;

		return this;
	}

	/**
	 * Check if value defined
	 *
	 * @returns {boolean}
	 * @memberof ConfigService
	 */
	public has(key: T): boolean {
		return this.config.hasOwnProperty(key);
	}

	/**
	 * Get config variable value
	 */
	public get(key: T, defaultValue?: string): string {
		const value = this.has(key) ? this.config[key] : defaultValue;
		const valueForReturn = this.shouldDecode(value) ? this.forceDecode(value) : value;
		try {
			return valueForReturn.trim();
		} catch (e) {
			return valueForReturn as any;
		}
	}

	/**
	 * Convenience method to get converted value via Number(val)
	 */
	public getNumber(key: T, defaultValue?: number): number {
		return Number(this.get(key) || defaultValue);
	}

	/**
	 * Convenience method to get parsed int value via parseInt(val, 10)
	 */
	public getInt(key: T, defaultValue?: number): number {
		return parseInt(this.get(key) || String(defaultValue), 10);
	}

	/**
	 * Convenience method to get parsed float value via parseFloat(val)
	 */
	public getFloat(key: T, defaultValue?: number): number {
		return parseFloat(this.get(key) || String(defaultValue));
	}

	/**
	 * Convenience method to get true or false value for 'true' or 'false'
	 * strings (case insensitive) or undefined
	 */
	public getBool(key: T, defaultValue?: boolean): boolean | undefined {
		const value = String(this.get(key) || String(defaultValue))
			.toLowerCase()
			.trim();

		if (value === 'true') {
			return true;
		}
		if (value === 'false') {
			return false;
		}

		return undefined;
	}

	/**
	 * Alias for getBool()
	 */
	public getBoolean(key: T, defaultValue?: boolean): boolean {
		return this.getBool(key, defaultValue) as any;
	}

	/**
	 * Convenience method to get difference from now to value in seconds
	 */
	public getPeriodSeconds(key: T, defaultValue?: string): number {
		return this.getMomentDuration(key, defaultValue).asSeconds();
	}

	/**
	 * Convenience method to get difference from now to value in milliseconds
	 */
	public getPeriodMs(key: T, defaultValue?: string): number {
		return this.getMomentDuration(key, defaultValue).asMilliseconds();
	}

	/**
	 * Convenience method to get converted value via URL
	 */
	public getUrl(key: T, defaultValue?: string): URL {
		return new URL(this.get(key) || String(defaultValue));
	}

	/**
	 * Convenience method to get converted value object via base64 encoded json
	 */
	public getJson(key: T, defaultValue?: object, validationDTO?: Type<any>, isArray?: boolean): object {
		const object = JSON.parse(this.get(key)) || defaultValue;
		if (validationDTO) {
			if (isArray) {
				if (!Array.isArray(object)) {
					throw new FatalException(`Array expected in ${key}`);
				}
				object.forEach((item: object, index: number) => {
					const validationIssues = validateSync(plainToClass(validationDTO, item));
					if (!isEmpty(validationIssues)) {
						throw new FatalException(`${key}[${index}] has validation errors by ${validationDTO.name}`, validationIssues);
					}
				});
			} else {
				const validationIssues = validateSync(plainToClass(validationDTO, object));
				if (!isEmpty(validationIssues)) {
					throw new FatalException(`${key} has validation errors by ${validationDTO.name}`, validationIssues);
				}
			}
		}
		return object;
	}

	/**
	 * Convenience method to get value from enum object
	 */
	public getEnumValue(key: T, objectValues: string[], defaultValue?: string): string {
		const value = this.get(key, defaultValue);
		const valueFromObject = objectValues.find((str: string) => str === value);
		if (!valueFromObject && !defaultValue) {
			throw new FatalException(`${value} not includes in ${objectValues.join(', ')}`);
		}
		if (!valueFromObject) {
			return value;
		}
		return valueFromObject;
	}

	/**
	 * Ensure variables are set, including common variables
	 * @throws Error
	 */
	public ensureConfig(configVars: object): void {
		const map: any = configVars;
		const missed = Object.keys(configVars).filter((name: any) => map[name] !== OPTIONAL && isNaN(name) && !this.has(name));
		if (missed.length) {
			throw new Error(`Config variables missed: ${missed.join(', ')}`);
		}
	}

	/**
	 *
	 * @private
	 * @param {string} value
	 * @returns {string}
	 * @memberof ConfigService
	 */
	private forceDecode(value: string): string {
		const [encoding, ...payload] = value.split(':');

		return this.handlers[encoding](payload.join(':'));
	}

	/**
	 *
	 * @private
	 * @param {string} value
	 * @returns {boolean}
	 * @memberof ConfigService
	 */
	private shouldDecode(value: string): boolean {
		if (typeof value !== 'string' || !value.substr(0, 16).includes(':')) {
			return false;
		}

		return this.handlers.hasOwnProperty(value.split(':', 1)[0]);
	}

	public getMomentDuration(key: T, defaultValue?: string): moment.Duration {
		const value = this.get(key, defaultValue);
		if (!value) {
			throw new FatalException(`${key} has empty value`);
		}
		const minusDirection = String(value).startsWith('-');
		const [valueCount, valuePeriod = 'day'] = String(value).replace(/[-+]/g, '').split(' '); // need for setup default period
		// eslint-disable-next-line @typescript-eslint/prefer-regexp-exec
		const matches = `${valueCount} ${valuePeriod}`.match(this.durationMatchRegex);
		if (!matches) {
			throw new FatalException(`${key} doesn't match regular expression ${this.durationMatchRegex.toString()}`);
		}
		const [, count, period] = matches as [string, string | number, moment.DurationInputArg2];
		return moment.duration(Number(minusDirection ? `-${count}` : count), period);
	}
}
