import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const ClientJwt = createParamDecorator((data: unknown, ctx: ExecutionContext): any => ctx.switchToHttp().getRequest().user);
