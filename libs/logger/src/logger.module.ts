import { DynamicModule, Module, Provider } from '@nestjs/common';
import { ModuleMetadata } from '@nestjs/common/interfaces';

import { FACTORY, LOGGER_CONFIG } from './logger.constants';
import defaults from './logger.defaults';
import { LoggerHelper } from './logger.helper';
import { LoggerService } from './logger.service';
import { LoggerConfigOptions, Logger, LoggerOptions } from './logger.types';

type InnerLoggerFactory = (options?: LoggerOptions) => Logger;

type LoggerFactory = (...args: any[]) => Promise<InnerLoggerFactory>;

interface LoggerModuleOptions extends Pick<ModuleMetadata, 'imports'> {
	loggerConfig?: LoggerConfigOptions;
	useFactory?: LoggerFactory; // supports inject
	useValue?: InnerLoggerFactory;
	inject?: any[];
}

@Module({
	exports: [LoggerService],
	providers: [LoggerHelper, LoggerService],
})
export class LoggerModule {
	public static registerAsync(options: LoggerModuleOptions): DynamicModule {
		return {
			module: LoggerModule,
			imports: options.imports || [],
			providers: [this.createLoggerProvider(options), this.createConfigOptionsProvider(options)],
		};
	}

	private static createLoggerProvider({ useFactory, useValue, inject }: LoggerModuleOptions): Provider {
		return {
			provide: FACTORY,
			inject,
			useValue,
			useFactory,
		};
	}

	private static createConfigOptionsProvider({ loggerConfig = {} }: LoggerModuleOptions): Provider {
		return {
			provide: LOGGER_CONFIG,
			useValue: {
				...defaults,
				...loggerConfig,
			},
		};
	}
}
