import { LoggerOptions } from './logger.types';

interface RegistryItem {
	token: string;
	options: any;
}

const registry: RegistryItem[] = [];

export class LoggerModuleRegistry {
	public static addLoggerProvider(token: string, options?: string | LoggerOptions): void {
		registry.push({ token, options });
	}

	public static get registry(): RegistryItem[] {
		return registry;
	}
}
