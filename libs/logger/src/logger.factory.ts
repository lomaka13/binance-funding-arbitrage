import * as _ from 'lodash';
import * as log4js from 'log4js';

import { AppCommonVarsType } from '@binance-test/app';
import { ConfigService } from '@binance-test/config';

import { Logger, LoggerOptions } from './logger.types';

/**
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Cyclic_object_value#Examples
 */
export const getCircularReplacer = (): ((this: any, _0: string, value: any) => void | any) => {
	const seen = new WeakSet();
	return function (this: any, _0: string, value: any): void {
		if (typeof value === 'object' && value !== null) {
			if (seen.has(value)) {
				return;
			}
			seen.add(value);
		}
		return value;
	};
};

export const jsonLayout =
	(config: any = {}): ((logEvent: log4js.LoggingEvent) => string) =>
	(logEvent: log4js.LoggingEvent): string =>
		JSON.stringify(
			{
				app: logEvent.context.app,
				categoryName: logEvent.categoryName,
				env: logEvent.context.env,
				level: logEvent.level,
				service: logEvent.context.service,
				startTime: logEvent.startTime,
				version: logEvent.context.version,
				data: logEvent.data[0],
			},
			getCircularReplacer(),
			config.pretty ? `\t` : undefined,
		);

/**
 * Creates log4js instance with bound json layout
 */
export const createLoggerSync = (configService: ConfigService<AppCommonVarsType>): ((options?: LoggerOptions) => Logger) => {
	const parts = String(configService.get('APP_NAME', 'wlx-common')).split('-');
	const app = parts[0];
	const service = parts.slice(1).join('-') || 'common';
	const version = configService.get('APP_VERSION');

	log4js.addLayout('json', jsonLayout);

	log4js.configure({
		appenders: {
			app: {
				type: 'stdout',
				layout: {
					type: 'json',
					pretty: configService.isLocal,
				},
			},
		},
		categories: {
			default: {
				appenders: ['app'],
				level: configService.get('APP_LOG_LEVEL', 'all'),
			},
		},
	});

	return (options: LoggerOptions = {}): Logger => {
		const { name, skip } = options;
		const logger = log4js.getLogger(name);

		logger.addContext('app', app);
		logger.addContext('service', service);
		logger.addContext('version', version);
		logger.addContext('env', configService.env);

		if (skip) {
			const log = logger.log.bind(logger);
			const rules = skip.map((entry: any) => Object.entries(entry));
			logger.log = (level: any, data: any): void => {
				const filter = rules.some((rule: (string | unknown)[]) =>
					rule.every(([path, match]: any) => {
						const value = _.get(data, path, '');
						return match instanceof RegExp ? match.test(value) : match === String(value);
					}),
				);
				if (!filter) {
					log(level, data);
				}
			};
		}

		return logger;
	};
};

export const createLogger = async (configService: ConfigService<AppCommonVarsType>): Promise<ReturnType<typeof createLoggerSync>> =>
	Promise.resolve(createLoggerSync(configService));
