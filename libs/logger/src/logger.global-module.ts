import { DynamicModule, Global, Module } from '@nestjs/common';

import { BootServiceInterface } from '@binance-test/boot';

import { LoggerService } from '.';
import { LoggerModuleRegistry } from './logger.module-registry';
import { loggerOptionsProviders, loggerProviders } from './logger.providers';

@Global()
@Module({})
export class LoggerGlobalModule {
	static forRoot(bootService: BootServiceInterface): DynamicModule {
		const loggerService = bootService.getLoggerService();
		const list = LoggerModuleRegistry.registry;
		const optionsProviders = loggerOptionsProviders(list);
		const providers = loggerProviders(loggerService, list);

		return {
			module: LoggerGlobalModule,
			providers: [
				{
					provide: LoggerService,
					useValue: loggerService,
				},
				...optionsProviders,
				...providers,
			],
			exports: [LoggerService, ...providers],
		};
	}
}
