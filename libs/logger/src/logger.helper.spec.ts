import { LoggerHelper } from './logger.helper';

describe('Common > logger > logger helper', () => {
	it('should create logger helper', () => {
		const helper = new LoggerHelper({});
		expect(helper).toBeDefined();
		expect(helper.toObject).toBeDefined();
	});

	it('should form log message', () => {
		const helper = new LoggerHelper({});
		const data = {
			type: 'log_type',
			message: 'log message',
		};
		const log = helper.toObject(data);
		expect(log).toEqual(log);
	});

	it('should add log type', () => {
		const helper = new LoggerHelper({});
		const data = {
			message: 'log message',
		};
		const log = helper.toObject(data);
		expect(log).toEqual({
			...data,
			type: 'log_message',
		});
	});

	it('should process non-safe fields', () => {
		const helper = new LoggerHelper({
			nonSafeDataFields: ['data'],
		});
		const data = {
			type: 'log_type',
			message: 'log message',
			data: {
				foo: 'bar',
				list: [1, 3, 4],
			},
		};
		const log = helper.toObject(data);
		expect(log).toEqual({
			type: data.type,
			message: data.message,
			['data:safe']: `{ foo: 'bar', list: [ 1, 3, 4 ] }`,
		});
	});

	it('should process an array with a single element', () => {
		const helper = new LoggerHelper({});
		const data = [{ message: 'log message' }];
		const log = helper.toObject(data);
		expect(log).toEqual({
			type: 'log_message',
			message: 'log message',
		});
	});

	it('should process an array', () => {
		const helper = new LoggerHelper({});
		const data = ['elem1', 'elem2'];
		const log = helper.toObject(data);
		expect(log).toEqual({
			type: 'malformed_log_message',
			message: `[ 'elem1', 'elem2' ]`,
		});
	});

	it('should process a string', () => {
		const helper = new LoggerHelper({});
		const data = 'log message';
		const log = helper.toObject(data);
		expect(log).toEqual({
			type: 'log_message',
			message: `log message`,
		});
	});

	it('should respect max length for non-safe fields', () => {
		const helper = new LoggerHelper({
			nonSafeDataFields: ['field1', 'field2'],
			maxStringLength: 20,
		});
		const data = {
			type: 'log_type',
			field1: '1 looooooonnnnng value',
			field2: '2 looooooonnnnng value',
		};
		const log = helper.toObject(data);
		expect(log).toEqual({
			type: 'log_type',
			['field1:safe']: `'1 looo...`,
			['field2:safe']: `'2 looo...`,
		});
	});
});
