const prefix = 'binancetest.server.logger';

export const getLoggerToken = (): string => `${prefix}.Logger.${Math.random()}`;

export const getLoggerOptionsToken = (loggerToken: string): string => `${loggerToken}.options`;
