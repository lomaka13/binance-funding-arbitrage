/**
 * Logger config options
 */
export interface LoggerConfigOptions {
	inspectDepth?: number;
	inspectMaxArrayLenght?: number;
	maxStringLength?: number;
	nonSafeDataFields?: string[];
}

/**
 * Logger options interface
 */
export interface LoggerOptions {
	name?: string;
	skip?: {
		[path: string]: RegExp | string;
	}[];
}

/**
 * Logger service interface
 */
export interface Logger {
	trace: (message: LogMessage) => void;
	debug: (message: LogMessage) => void;
	info: (message: LogMessage) => void;
	warn: (message: LogMessage) => void;
	error: (message: LogMessage) => void;
	fatal: (message: LogMessage) => void;
}

/**
 * Log level names
 */
export type LogLevel = keyof Logger;

/**
 * Fields of this type will be converted to string with util.inspect
 */
type SafeLogData = any;

interface LogMessageSafe {
	data?: SafeLogData;
	result?: SafeLogData;
	error?: SafeLogData;
	query?: SafeLogData;
	body?: SafeLogData;
	params?: SafeLogData;
}

export type LogMessageSafeField = keyof LogMessageSafe;

export interface LogMessageInterface {
	type: string;
	rid?: string;
	context?: string;
	trace?: string;
	[field: string]: any;
}

export type LogMessage = LogMessageInterface & LogMessageSafe & { message: string };

export enum LogType {
	LOG_MESSAGE = 'log_message',
	MALFORMED_LOG_MESSAGE = 'malformed_log_message',
	NEST_MESSAGE = 'nest_message',

	HTTP_OK = 'http_ok',
	HTTP_OK_EXT = 'http_ok_ext',
	HTTP_ERROR = 'http_error',
	HTTP_ERROR_EXT = 'http_error_ext',
}
