import { LoggerConfigOptions } from './logger.types';

const options: LoggerConfigOptions = {
	inspectDepth: 3,
	inspectMaxArrayLenght: 12,
	maxStringLength: 2048,
	nonSafeDataFields: ['result', 'error', 'params', 'json'],
};

export default options;
