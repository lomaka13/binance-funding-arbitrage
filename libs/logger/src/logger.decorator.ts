import { Inject } from '@nestjs/common';

import { LoggerModuleRegistry } from './logger.module-registry';
import { getLoggerToken } from './logger.tokens';
import { LoggerOptions } from './logger.types';

export const InjectLogger = (options?: string | LoggerOptions): any => {
	const token = getLoggerToken();
	LoggerModuleRegistry.addLoggerProvider(token, options);
	return Inject(token);
};
