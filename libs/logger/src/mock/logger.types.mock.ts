import { Logger } from '@binance-test/logger';

export interface LoggerMockApi {
	mockClear: () => void;
	mockReset: () => void;
	getCallsCount: () => number;
	getCalls: () => any[];
	getResults: () => any[];
}

export type LoggerMock = Record<keyof Logger, jest.Mock> & LoggerMockApi;
