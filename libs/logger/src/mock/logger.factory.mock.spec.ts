import { random } from 'faker';

import { createLoggerMock } from './logger.factory.mock';

describe('Common > logger > logger factory mock', () => {
	afterEach(() => {
		jest.resetAllMocks();
	});

	it('should create logger mock', () => {
		const logger = createLoggerMock();
		expect(logger).toBeDefined();
		expect(logger.info).toBeCalledTimes(0);
		expect(logger.error).toBeCalledTimes(0);
		expect(logger.warn).toBeCalledTimes(0);
		expect(logger.debug).toBeCalledTimes(0);
		expect(logger.trace).toBeCalledTimes(0);
		expect(logger.fatal).toBeCalledTimes(0);
	});

	it('should create logger mock with predefined methods', () => {
		const fn = jest.fn();
		const logger = createLoggerMock({
			debug: fn,
		});
		const params = { type: 'mock_test', num: 1 };
		logger.debug(params);
		expect(logger.debug).toBeCalledTimes(1);
		expect(logger.debug).toHaveBeenCalledWith(params);
	});

	it(`should 'mockClear' clear all methods mock`, () => {
		const logger = createLoggerMock();
		logger.debug({});
		logger.error({});
		expect(logger.debug).toBeCalledTimes(1);
		expect(logger.error).toBeCalledTimes(1);
		logger.mockClear();
		expect(logger.debug).toBeCalledTimes(0);
		expect(logger.error).toBeCalledTimes(0);
	});

	it(`should 'mockReset' reset all methods mock`, () => {
		const logger = createLoggerMock();
		logger.debug.mockReturnValue(123);
		logger.error.mockReturnValue(456);
		expect(logger.debug()).toEqual(123);
		expect(logger.error()).toEqual(456);
		logger.mockReset();
		expect(logger.debug()).toBeUndefined();
		expect(logger.error()).toBeUndefined();
	});

	it(`should 'getCallsCount' return total calls count`, () => {
		const logger = createLoggerMock();
		logger.trace();
		logger.trace();
		logger.info();
		logger.error();
		expect(logger.getCallsCount()).toEqual(4);
	});

	it(`should 'getCalls' return combined calls info`, () => {
		const logger = createLoggerMock();
		const call1 = random.words();
		const call2 = random.words();
		const call3 = random.words();
		logger.trace(call1);
		logger.trace(call1);
		logger.info(call2);
		logger.error(call3);
		expect(logger.getCalls()).toEqual([[call1], [call1], [call2], [call3]]);
	});

	it(`should 'getResults' return combined calls info`, () => {
		const logger = createLoggerMock();
		const call1 = random.words();
		const call2 = random.words();
		const call3 = random.words();
		logger.trace.mockReturnValue(call1);
		logger.info.mockReturnValue(call2);
		logger.error.mockReturnValue(call3);
		logger.trace();
		logger.trace();
		logger.info();
		logger.error();
		expect(logger.getResults()).toEqual(
			[call1, call1, call2, call3].map((value: any) => ({
				type: 'return',
				value,
			})),
		);
	});
});
