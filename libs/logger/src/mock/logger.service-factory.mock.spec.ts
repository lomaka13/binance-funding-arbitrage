import { random } from 'faker';

import { createLoggerServiceMock, LoggerServiceMock } from './logger.service-factory.mock';

describe('Common > logger > logger service factory mock', () => {
	let loggerService: LoggerServiceMock;

	beforeEach(() => {
		loggerService = createLoggerServiceMock();
	});

	afterEach(() => {
		loggerService.clearMocks();
		jest.resetAllMocks();
	});

	it('should create logger service mock', () => {
		expect(loggerService).toBeDefined();
		expect(loggerService.getLogger).toBeDefined();
		expect(loggerService.wrapRequest).toBeDefined();
		expect(loggerService.getMock).toBeDefined();
		expect(loggerService.getMocks).toBeDefined();
		expect(loggerService.clearMocks).toBeDefined();
	});

	it('should create logger service mock with predefined logger mock methods', () => {
		const fn = jest.fn();
		const service = createLoggerServiceMock({
			debug: fn,
		});
		const logger = service.getLogger();
		const params = { type: 'mock_test', num: 1 };
		logger.debug(params);
		expect(logger.debug).toStrictEqual(fn);
		expect(fn).toBeCalledTimes(1);
		expect(fn).toHaveBeenCalledWith(params);
	});

	it('should mock wrap request', () => {
		const logger = loggerService.wrapRequest(random.uuid(), 'logger-wrap');
		expect(logger).toStrictEqual(loggerService.getMock('logger-wrap'));
	});

	it('should return loggers mocks', () => {
		const firstLogger = loggerService.getLogger('first-logger');
		const secondLogger = loggerService.getLogger('second-logger');
		expect(firstLogger).toStrictEqual(loggerService.getMock('first-logger'));
		expect(secondLogger).toStrictEqual(loggerService.getMock('second-logger'));
	});

	it('should return all loggers mocks', () => {
		const firstLogger = loggerService.getLogger('first-logger');
		const secondLogger = loggerService.getLogger('second-logger');
		expect(loggerService.getMocks()).toStrictEqual({
			// eslint-disable-next-line @typescript-eslint/naming-convention
			'first-logger': firstLogger,
			// eslint-disable-next-line @typescript-eslint/naming-convention
			'second-logger': secondLogger,
		});
	});
});
