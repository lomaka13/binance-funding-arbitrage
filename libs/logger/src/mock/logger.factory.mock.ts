import { map as pluck, invokeMap, flatten } from 'lodash';

import { Logger } from '@binance-test/logger';

import { LoggerMock, LoggerMockApi } from './logger.types.mock';

const createMock = (): Record<keyof Logger, jest.Mock> => ({
	trace: jest.fn(),
	debug: jest.fn(),
	info: jest.fn(),
	warn: jest.fn(),
	error: jest.fn(),
	fatal: jest.fn(),
});

export const createLoggerMock = (partialMock: Partial<LoggerMock> = {}): LoggerMock => {
	const mock: Record<keyof Logger, jest.Mock> = Object.assign(createMock(), partialMock);
	const api: LoggerMockApi = {
		mockClear(): void {
			invokeMap(Object.values(mock), 'mockClear');
		},
		mockReset(): void {
			invokeMap(Object.values(mock), 'mockReset');
		},
		getCallsCount(): number {
			return flatten(pluck(Object.values(mock), 'mock.calls')).length;
		},
		getCalls(): any[] {
			return flatten(pluck(Object.values(mock), 'mock.calls').filter((calls: any[]) => calls.length));
		},
		getResults(): any[] {
			return flatten(pluck(Object.values(mock), 'mock.results').filter((results: any[]) => results.length));
		},
	};

	// @ts-ignore
	return Object.assign({}, mock, api);
};
