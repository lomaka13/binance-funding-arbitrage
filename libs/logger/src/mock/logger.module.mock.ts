// eslint-disable-next-line max-classes-per-file
import { Module, Provider, DynamicModule, Global } from '@nestjs/common';

import { LoggerService } from '../';
import { LoggerModuleRegistry } from '../logger.module-registry';

import { createLoggerServiceMock, LoggerServiceMock } from './logger.service-factory.mock';

@Module({})
export class LoggerModuleMock {
	public static register(): DynamicModule {
		const list = LoggerModuleRegistry.registry;
		const loggerService = createLoggerServiceMock();
		const loggerProviders = list.map((options: any) => this.getLoggerMockProvider(loggerService, options));
		const serviceProvider = {
			provide: LoggerService,
			useValue: loggerService,
		};
		const providers = [...loggerProviders, serviceProvider];
		return {
			module: LoggerModuleMock,
			providers,
			exports: providers,
		};
	}

	private static getLoggerMockProvider(loggerService: LoggerServiceMock, { token, options }: any): Provider {
		const logger = loggerService.getLogger(options);
		return {
			provide: token,
			useValue: logger,
		};
	}
}

@Global()
@Module({})
export class LoggerGlobalModuleMock extends LoggerModuleMock {
	public static register(): DynamicModule {
		return {
			...super.register(),
			module: LoggerGlobalModuleMock,
		};
	}
}
