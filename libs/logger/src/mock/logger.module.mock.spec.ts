import { Test, TestingModule } from '@nestjs/testing';

import { LoggerService, InjectLogger, Logger } from '../';

import { LoggerModuleMock, LoggerServiceMock } from './';

class SomeService {
	constructor(
		public loggerService: LoggerService,
		@InjectLogger() public defaultLogger: any,
		@InjectLogger('loggerName') public logger: any,
		@InjectLogger({ name: 'configuredLogger' }) public configuredLogger: any,
	) {}

	public getLogger(name: string): Logger {
		return this.loggerService.getLogger(name);
	}

	public doLog(): void {
		this.defaultLogger.info('defaultLogger: done something');
		this.logger.info('logger: done something');
		this.configuredLogger.info('configuredLogger: done something');
	}
}

describe('Common > logger > logger module mock', () => {
	let service: SomeService;
	let loggerService: LoggerServiceMock;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			imports: [LoggerModuleMock.register()],
			providers: [SomeService],
		}).compile();

		service = module.get(SomeService);
		loggerService = service.loggerService as any;
	});

	afterEach(() => {
		jest.clearAllMocks();
	});

	it('should mock logger service', () => {
		expect(service.loggerService).toBeDefined();
	});

	it('logger service should factory loggers mocks', () => {
		const logger = service.loggerService.getLogger('new-logger');
		expect(logger).toBeDefined();
		expect(service.getLogger('new-logger')).toBe(logger);
	});

	it('should inject loggers', () => {
		expect(service.defaultLogger).toBeDefined();
		expect(service.logger).toBeDefined();
		expect(service.configuredLogger).toBeDefined();
	});

	it('should provide loggers mocks for tests', () => {
		expect(loggerService.getMock('default')).toBe(service.defaultLogger);
		expect(loggerService.getMock('loggerName')).toBe(service.logger);
		expect(loggerService.getMock('configuredLogger')).toBe(service.configuredLogger);
	});

	it('should provide loggers methods for tests', () => {
		service.doLog();
		expect(loggerService.getMock('default').info).toBeCalledWith('defaultLogger: done something');
		expect(loggerService.getMock('loggerName').info).toBeCalledWith('logger: done something');
		expect(loggerService.getMock('configuredLogger').info).toBeCalledWith('configuredLogger: done something');
	});
});
