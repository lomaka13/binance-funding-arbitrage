import { LoggerService } from '../logger.service';

import { createLoggerMock } from './logger.factory.mock';
import { LoggerMock } from './logger.types.mock';

interface MocksMap {
	[name: string]: LoggerMock;
}

const mocks: MocksMap = {};

const getName = (name: string | { name: string } = 'default'): string => {
	if (typeof name === 'string') {
		return name;
	}
	return name.name;
};

export type LoggerServiceMock = Record<keyof LoggerService, jest.Mock> & {
	getMock: (name: string) => LoggerMock;
	getMocks: () => MocksMap;
	clearMocks: () => void;
};

export const createLoggerServiceMock = (partialMock: Partial<LoggerMock> = {}): LoggerServiceMock => ({
	getLogger: jest.fn((options?: any) => {
		const name = getName(options);
		mocks[name] = mocks[name] || createLoggerMock(partialMock);
		return mocks[name];
	}),
	wrapRequest: jest.fn(function (this: any, rid: string, options?: any): any {
		return this.getLogger(options);
	}),
	getMock: (name: string): LoggerMock => mocks[name],
	getMocks: (): MocksMap => mocks,
	clearMocks: (): void => {
		Object.keys(mocks).forEach((key: string) => delete mocks[key]);
	},
});
