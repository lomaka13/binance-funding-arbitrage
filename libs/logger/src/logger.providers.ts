import { Provider } from '@nestjs/common';

import { LoggerService } from './logger.service';
import { getLoggerOptionsToken } from './logger.tokens';
import { Logger } from './logger.types';

export const loggerOptionsProviders = (optionsList: { token: string; options: any }[]): Provider[] => {
	return optionsList.map(({ token, options }: { token: string; options: any }) => ({
		provide: getLoggerOptionsToken(token),
		useValue: options,
	}));
};

export const loggerProviders = (loggerService: LoggerService, optionsList: { token: string; options: any }[]): Provider[] => {
	return optionsList.map(({ token }: { token: string }) => ({
		provide: token,
		useFactory: (options: any): Logger => loggerService.getLogger(options),
		inject: [getLoggerOptionsToken(token)],
	}));
};
