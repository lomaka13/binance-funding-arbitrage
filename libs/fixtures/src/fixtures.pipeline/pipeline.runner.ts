import { isMatch } from './pipeline.matcher';
import { PipelinePattern, HandlerResult } from './pipeline.types';

/**
 * Will modify and return 'result' object
 */
export async function runPipeline<P, T>(patterns: PipelinePattern<P, T>[], matcher: object, params: P, initialResult: HandlerResult<T>): Promise<T> {
	let result: any = initialResult;
	for (const pattern of patterns) {
		if (isMatch(pattern.pattern || {}, matcher)) {
			result = await pattern.handler(params as any, result);
			if (pattern.break) {
				break;
			}
		}
	}
	return result;
}
