import { isObject } from 'lodash';

import { MATCH_NOT, MATCH_IS_IN, MATCH_NOT_IS_IN, MATCH_INSTANCE_OF, MATCH_NOT_INSTANCE_OF } from './pipeline.constants';

export function isMatch(pattern: object, obj: object): boolean {
	let isEqual = true;
	for (const key of Object.keys(pattern)) {
		const patternItem = (pattern as any)[key];
		const value = (obj as any)[key];

		if (patternItem?.[MATCH_INSTANCE_OF] !== undefined) {
			isEqual = value instanceof patternItem?.[MATCH_INSTANCE_OF];
		} else if (patternItem?.[MATCH_NOT_INSTANCE_OF] !== undefined) {
			isEqual = !(value instanceof patternItem?.[MATCH_NOT_INSTANCE_OF]);
		} else if (patternItem?.[MATCH_IS_IN] !== undefined) {
			isEqual = patternItem?.[MATCH_IS_IN].includes(value);
		} else if (patternItem?.[MATCH_NOT_IS_IN] !== undefined) {
			isEqual = !patternItem?.[MATCH_NOT_IS_IN].includes(value);
		} else if (patternItem?.[MATCH_NOT] !== undefined) {
			isEqual = patternItem?.[MATCH_NOT] !== value;
		} else {
			if (isObject(patternItem)) {
				if (!value) {
					return false;
				}
				isEqual = isMatch(patternItem, value);
			} else {
				isEqual = patternItem === value || (patternItem === '*' && value !== undefined);
			}
		}
		if (!isEqual) {
			return false;
		}
	}
	return true;
}
