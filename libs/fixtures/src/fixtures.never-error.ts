/**
 * Type-checking error indicating prducing statement should be unreachable
 */
export class NeverError extends Error {
	constructor(value: never, message?: string) {
		super(message || `Value '${value}' is of never type`);
	}
}
