export { IntersectionType, PickType, PartialType, OmitType } from '@nestjs/swagger';
export { Type } from '@nestjs/common';

export type Await<T> = T extends {
	then(onfulfilled?: (value: infer U) => unknown): unknown;
}
	? U
	: T;

export type ParameterType<T, M extends keyof T> = T[M];

export type TypeOfClassMethod<T, M extends keyof T> = T[M] extends (...args: any) => any ? T[M] : never;

export type ReturnTypeOfClassMethod<T, M extends keyof T> = ReturnType<TypeOfClassMethod<T, M>>;

export type ParameterOfClassMethod<T, M extends keyof T, P extends number> = Parameters<TypeOfClassMethod<T, M>>[P];

export type Without<T, U> = { [P in Exclude<keyof T, keyof U>]?: never };

export type TypeXOR<T, U> = T | U extends object ? (Without<T, U> & U) | (Without<U, T> & T) : T | U;

export type ElementOfArray<T> = T extends (infer U)[] ? U : never;

export interface ObjectLiteral {
	[key: string]: any;
}

export type DeepPartial<T, Except = never> = T extends Except ? T : T extends object ? { [K in keyof T]?: DeepPartial<T[K], Except> } : T;

export type DeepRequired<T, Except = never> = T extends Except ? T : T extends object ? Required<{ [P in keyof T]: DeepRequired<T[P], Except> }> : T;

export type ClassType<T> = new (...args: any[]) => T;

export interface Dictionary<T> {
	[index: string]: T;
}

export type KnownKeys<T> = {
	[K in keyof T]: string extends K ? never : number extends K ? never : K;
} extends { [_ in keyof T]: infer U }
	? U
	: never;

export type PickKnownKeys<T> = Pick<T, KnownKeys<T>>;
