interface ValueTypeMap {
	['string']: string;
	['number']: number;
	bigint: bigint;
	['boolean']: boolean;
	symbol: symbol;
	object: object;
	['undefined']: undefined;
	function: (...ags: any[]) => any;
}
/**
 * Checks if object of type 'unknown' is of type 'T' by discriminant property type
 */
export const unknownHasProp = <T extends object>(obj: any, key: PropertyKey, type: keyof ValueTypeMap): obj is T =>
	obj instanceof Object && key in obj && typeof obj[key] === type;

/**
 * Checks if object of type 'unknown' is of type 'T' by dicscriminant property value
 */
export const unknownPropEq = <T extends object>(obj: any, key: PropertyKey, value: any): obj is T => obj instanceof Object && obj[key] === value;

/**
 * Return boolean from bool like values
 */
export const unknownToBool = (val: unknown): boolean => {
	const value = String(val).toLowerCase().trim();

	return ['true', '1'].includes(value);
};

/**
 * Get normalized object property
 */
export const unknownGetProp = <T>(obj: any, key: string): T => {
	return obj instanceof Object && obj[Object.keys(obj).find((k: string) => k.toLowerCase() === key.toLowerCase())];
};
