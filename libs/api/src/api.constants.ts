import { HttpStatus as HttpStatusCommon } from '@nestjs/common';

export const HttpStatus = {
	...HttpStatusCommon,
	PRECONDITION_REQUIRED: 428,
	RETRY_WITH: 449,
};
