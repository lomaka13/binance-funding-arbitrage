import { ApiProperty } from '@nestjs/swagger';

import { IsTimestamp } from '@binance-test/validation';

export class DatesDto {
	@ApiProperty({ description: 'date in timestamp format' })
	@IsTimestamp()
	fromDate: number;

	@ApiProperty({ description: 'date in timestamp format' })
	@IsTimestamp()
	tillDate: number;
}
