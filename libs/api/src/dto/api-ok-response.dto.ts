// eslint-disable-next-line max-classes-per-file
import { ApiProperty, ApiPropertyOptions } from '@nestjs/swagger';

import { Type } from '@binance-test/fixtures';

export abstract class ApiOkResponseDto<T> {
	@ApiProperty({
		description: 'HTTP status code',
	})
	status: number;

	@ApiProperty({
		description: 'Response data',
	})
	data: T;
}

export const ApiOkResponseType = <T = any>(options: ApiPropertyOptions): Type<ApiOkResponseDto<T>> => {
	class ResponseClass extends ApiOkResponseDto<T> {
		@ApiProperty(options)
		data: T;
	}
	return ResponseClass;
};
