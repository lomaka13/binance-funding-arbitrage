import { ApiProperty } from '@nestjs/swagger';

export class ListPaginationDto {
	@ApiProperty()
	limit: number;

	@ApiProperty()
	page: number;

	@ApiProperty()
	offset: number;
}
