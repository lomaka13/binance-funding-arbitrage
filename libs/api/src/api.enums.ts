export enum RequestHeader {
	REQUEST_ID = 'x-binancetest-request-id',
	RESPONSE_DATA_LOG = 'x-binancetest-response-data-log',
}

export enum ResponseHeader {
	REQUEST_ID = 'x-binancetest-request-id',
}

export enum API_VERSION {
	API = 'api',
	DEV = 'dev',
}
