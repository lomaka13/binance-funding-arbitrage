import { Controller, applyDecorators } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { capitalize } from 'lodash';

import { DevOnly } from '@binance-test/validation';

import { API_VERSION } from './api.enums';

export const ApiController = (path: string, ignoredClientVersionMethods?: string[]): ReturnType<typeof applyDecorators> => {
	return applyDecorators(Controller({ path, version: API_VERSION.API }), ApiTags(capitalize(path)));
};

export const DevController = (path?: string): ReturnType<typeof applyDecorators> => {
	return applyDecorators(Controller({ path, version: API_VERSION.DEV }), ApiTags(capitalize(path)), DevOnly);
};
