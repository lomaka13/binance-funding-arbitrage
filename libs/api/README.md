# Client version checker

### Usage example

Inject components

```ts
import { Get } from '@nestjs/common';

import {
  ApiController,
  BoController,
  DevController,
} from '@binance-test/api';

@ApiController('1') // = /api/1
export class AppController1 {
  @Get('info') // /api/1/info
  info() {}
}

@BoController('1') // = /bo/1
export class AppController2 {
  @Get('info') // = /bo/1/info
  info() {}
}

@DevController('1') // = /dev/1 - dev only controller
export class AppController2 {
  @Get('info') // = /dev/1/info
  info() {}
}
```

