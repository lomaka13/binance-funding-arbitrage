// eslint-disable-next-line max-classes-per-file
import { omit, zipObject } from 'lodash';

import { unknownHasProp } from '@binance-test/fixtures';

import { ErrorCode } from './exceptions.enums';
import { ExceptionInfo } from './exceptions.types';

const errorCodes: { [code in ErrorCode]: void } = zipObject(Object.values(ErrorCode)) as any;
const isErrorCode = (value: unknown): value is ErrorCode => typeof value === 'string' && errorCodes.hasOwnProperty(value);

/**
 * General (ROOT) application exception
 */
export class AppException extends Error {
	/**
	 * Business logic error code
	 */
	protected errCode?: ErrorCode;

	public get code(): ErrorCode | undefined {
		return this.errCode;
	}

	/**
	 * Additional exception data to for public api clients
	 * WARNING: Shouldn't contain sensitive data
	 */
	protected data?: any;

	/**
	 * Log-only additional error info, not to be returned to clients
	 */
	protected originalError?: any;

	constructor(message: string, codeOrError?: ErrorCode | any) {
		super(message);
		this.name = this.constructor.name;
		this.setExtra(codeOrError);
	}

	/**
	 * Get exception info for clients
	 */
	public getInfo(): ExceptionInfo {
		const info: ExceptionInfo = {
			...(this.code && { code: this.code }),
			name: this.name,
			message: this.message,
			...(this.data && { data: this.data }),
		};
		return info;
	}

	public getOriginalError(): any | undefined {
		return this.originalError;
	}

	private setExtra(codeOrError?: ErrorCode | any): void {
		if (!codeOrError) {
			return;
		}
		if (isErrorCode(codeOrError)) {
			this.errCode = codeOrError;
			return;
		}
		if (isErrorCode(codeOrError?.code)) {
			this.errCode = codeOrError.code;
		}
		this.originalError = codeOrError;
	}
}

/**
 * Fatal exception should lead to service process exit
 */
export class FatalException extends AppException {}

/**
 * Base exception with extended message construcctor params support
 */
abstract class ExtendedMessageException extends AppException {
	constructor(data: unknown | string, codeOrError?: ErrorCode | any) {
		let message = 'Bad params exception';
		let extraData: any;
		if (typeof data === 'string') {
			message = data;
		} else if (typeof data === 'object') {
			if (unknownHasProp<{ message: string }>(data, 'message', 'string')) {
				message = data.message;
			}
			extraData = omit(data, 'message');
		}
		super(message, codeOrError);
		this.data = extraData;
	}
}

/**
 * Exception representing bad input params error
 */
export class BadParamsException extends ExtendedMessageException {}

/**
 * Exception for unprocessable requests
 */
export class UnprocessableException extends ExtendedMessageException {
	constructor(data: unknown | string, code: ErrorCode = ErrorCode.UNPROCESSABLE_ENTITY) {
		super(data, code);
	}
}

/**
 * Auth exception
 */
export class AuthException extends AppException {
	constructor(message: string, code: ErrorCode = ErrorCode.AUTH_EXCEPTION, data?: any) {
		super(message, code);
		this.data = data;
	}
}

/**
 * Conflict exception
 */
export class ConflictException extends AppException {
	constructor(message: string, code: ErrorCode, data?: any) {
		super(message, code);
		this.data = data;
	}
}

/**
 * Timeout exception
 */
export class TimeoutException extends AppException {}

/**
 * Not exist exception
 */
export class NotExistsException extends AppException {
	constructor(message?: string, code: ErrorCode = ErrorCode.NOT_EXISTS) {
		super(message || 'Requested entity not found', code);
	}
}

/**
 * Record being updated has changed since processed version
 */
export class RecordChangedException extends AppException {
	constructor(message: string) {
		super(message);
	}
}

/**
 * Duplicate record
 */
export class DuplicateRecordException extends AppException {
	constructor({ message, detail }: { message: string; detail: string }) {
		super(message);
		this.data = detail;
	}
}

/**
 * Service unavalilable exception, request should be re-tried later
 */
export class ServiceUnavailableException extends AppException {}

/**
 * Represents not implemented case.
 */
export class NotImplementedException extends AppException {}
