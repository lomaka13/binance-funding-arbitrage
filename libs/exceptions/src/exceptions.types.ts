import { ErrorCode } from './exceptions.enums';

export interface ExceptionInfo {
	code?: ErrorCode;
	name: string;
	message: string;
	data?: any;
}
