import * as request from 'supertest';
import { Server } from 'http';
import { INestApplication } from '@nestjs/common';

import { ConfigGlobalModule } from '@binance-test/config';
import { LoggerGlobalModule } from '@binance-test/logger';
import { LoggerGlobalModuleMock } from '@binance-test/mocks/logger';
import { initApp, initBoot } from '@binance-test/test-utils/bootstrap';
import '@binance-test/test-utils';

import { AppVars } from '../src/app/app.config.vars';
import { AppModule } from '../src/app/app.module';

import './test.env';

jest.setTimeout(30000);

describe.skip('App (e2e)', () => {
	let server: Server;
	let app: INestApplication;

	beforeAll(async () => {
		const bootService = await initBoot({
			configVars: AppVars,
			loggerModule: LoggerGlobalModuleMock.register(),
		});
		app = await initApp(bootService, {
			testModule: {
				imports: [
					ConfigGlobalModule.forRoot(bootService),
					LoggerGlobalModule.forRoot(bootService),
					AppModule.forRoot(bootService),
				],
			},
		});

		server = app.getHttpServer();

		await app.init();
	});

	afterAll(async () => {
		await app.close();
	});
});
